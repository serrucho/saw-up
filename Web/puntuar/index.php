﻿<?php
error_reporting(0);
 $host_db = "localhost";
 $user_db = "admin";
 $pass_db = "admin";
 $db_name = "sawup";
 $conexion = new mysqli($host_db, $user_db, $pass_db, $db_name);
 if ($conexion->connect_error) {
 die("La conexion falló: " . $conexion->connect_error);
 }
 
$result = $conexion->query("SELECT IFNULL(CONCAT(ROUND(AVG(opinion.puntuacion),1), '/10'), '0/10') FROM opinion");
$row = $result->fetch_row();
echo "<!DOCTYPE HTML>\n" .
"<!--\n" .
"	Alpha by HTML5 UP\n" .
"	html5up.net | @ajlkn\n" .
"	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)\n" .
"-->\n" .
"<html>\n" .
"	<head>\n" .
"		<title>Saw Up - Tu gestor de copias de seguridad</title>\n" .
"		<meta charset=\"utf-8\" />\n" .
"		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\" /> 
		<link  rel=\"icon\"   href=\"../favicon.png\" type=\"image/png\" />\n" .
"		<link rel=\"stylesheet\" href=\"../assets/css/main.css\" />\n" .
"	</head>\n" .
"	<body class=\"is-preload\">\n" .
"		<div id=\"page-wrapper\">\n" .
"<header id=\"header\">\n" .
"					<h1><a href=\"/\">Saw Up</a> - Tu gestor de copias de seguridad</h1>\n" .
"					<nav id=\"nav\">\n" .
"						<ul>\n" .
"							<li><a class=\"icon solid fa-home\" href=\"/\">Home</a></li>							\n" .
"						</ul>\n" .
"					</nav>\n" .
"				</header>\n" .
"\n
				 <section id=\"banner\">

		<h2><IMG SRC=\"../images/header.png\"></h2><header><header>\n" .
"						<h2>Nuestros usuarios nos han evaluado con una nota de ".$row[0]."</h2>\n" .
"						<p>¿Qué te parece Saw Up? Cuéntanoslo rellenando el siguiente formulario:</p>\n" .
"					</header></section>" .
"				<section id=\"main\" class=\"container medium\">				\n" .
"				\n" .
"					\n" .
"					<div class=\"box\">					\n" .
"					 <form action=\"registrarPuntuacion.php\" method=\"post\" name=\"formPunt\">\n" .
"							<div class=\"row gtr-50 gtr-uniform\">								\n" .
"								<div class=\"col-12\">\n" .
"									 <input type=\"email\" name=\"email\" id=\"email\" placeholder=\"Email\" title=\"Escribe tu correo electronico: ejemplo@ejemplo.com\" required/>\n" .
"								</div>\n" .
"								<div class=\"col-12\">\n" .
"										<select name=\"nota\" placeholder=\"HAFODM\">\n" .
"										<option disabled selected>Puntua con un 10 (más alto) o 1 (más bajo)</option>\n" .
"										<option>10</option>\n" .
"										<option>9</option>\n" .
"										<option>8</option>\n" .
"										<option>7</option>\n" .
"										<option>6</option>\n" .
"										<option>5</option>\n" .
"										<option>4</option>\n" .
"										<option>3</option>\n" .
"										<option>2</option>\n" .
"										<option>1</option>\n" .
"										</select>									\n" .
"									</div>								\n" .
"								<div class=\"col-12\">\n" .
"									<textarea name=\"mensaje\" placeholder=\"Cuéntanos que te parece la aplicación…\"></textarea>\n" .
"								</div>\n" .
"								<div class=\"col-12\">\n" .
"									<input type=\"checkbox\" id=\"cbox2\" value=\"second_checkbox\" required> <label for=\"cbox2\">Acepto los <a href=\"../terminos-condiciones\" target=\"_blank\">terminos y condiciones</a></label>\n" .
"								</div>\n" .
"								<div class=\"col-12\">\n" .
"									<ul class=\"actions special\">\n" .
"										<li><input class=\"botons\" type=\"submit\" value=\"Puntuar\"/></li>\n" .
"									</ul>\n" .
"								</div>\n" .
"							</div>\n" .
"						</form>\n" .
"					</div>\n" .
"				</section>\n" .
"				<footer id=\"footer\">					\n" .
"					<ul class=\"copyright\">\n" .
"						<li>Saw Up - Proyecto Final de Grado Superior de Desarrollo de Aplicaciones Multiplataforma</li>\n" .
"					</ul>\n" .
"				</footer>\n" .
"		</div>\n" .
"			<script src=\"assets/js/jquery.min.js\"></script>\n" .
"			<script src=\"assets/js/jquery.dropotron.min.js\"></script>\n" .
"			<script src=\"assets/js/jquery.scrollex.min.js\"></script>\n" .
"			<script src=\"assets/js/browser.min.js\"></script>\n" .
"			<script src=\"assets/js/breakpoints.min.js\"></script>\n" .
"			<script src=\"assets/js/util.js\"></script>\n" .
"			<script src=\"assets/js/main.js\"></script>\n" .
"\n" .
"	</body>\n" .
"</html>";

?>