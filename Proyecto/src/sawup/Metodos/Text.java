package sawup.Metodos;

import com.placeholder.PlaceHolder;
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import sawup.Clases.Paths;
import sawup.Hilos.RegistroEnvioError;
import sawup.Main;
import static sawup.Main.tblModificar;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Text {
    
    static final String fontPrincipal = "Microsoft JhengHei";
    static final int tamañoPricipal = 18;
    
    public static void text(PlaceHolder holder) {
        Properties p = new Properties();
        p = comprobarLenguaje(p);
        Main.lblRestarPass.setText(p.getProperty("lblRestarPass"));
        Main.btnInciarSesion.setText(p.getProperty("btnInciarSesion"));
        Main.btnRegistrar.setText(p.getProperty("btnRegistrar"));
        Main.btnRestartPass.setText(p.getProperty("btnRestartPass"));
        Main.btnCopiaLocal.setText(p.getProperty("btnCopiaLocal"));
        Main.btnCopiaEnLinea.setText(p.getProperty("btnCopiaEnLinea"));
        Main.btnCopiaRapida.setText(p.getProperty("btnCopiaRapida"));
        Main.btnClonar.setText(p.getProperty("btnClonar"));
        Main.btnContacto.setText(p.getProperty("btnContacto"));
        Main.btnEditarCopia.setText(p.getProperty("btnEditarCopia"));
        Main.btnConfiguracion.setText(p.getProperty("btnConfiguracion"));
        Main.lblMisUnidades.setText(p.getProperty("lblMisUnidades"));
        Main.btnRest.setText(p.getProperty("btnRest"));
        Main.tblUnidades.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("lblUniResumen"), p.getProperty("lblEspaDispResumen"), p.getProperty("lblEspaTotaResu")}) {
            Class[] types = new Class[]{
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.btnFormatear.setText(p.getProperty("btnFormatear"));
        Main.btnAbrir.setText(p.getProperty("btnAbrir"));
        Main.btnCorregir.setText(p.getProperty("btnCorregir"));
        Main.lblTareas.setText(p.getProperty("lblTareas"));
        Main.tblTareas.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("lblIdentifiResumen"), p.getProperty("lblDescrResumen")}) {
            Class[] types = new Class[]{
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false, false
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.tblRapiF.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("lblTablFichRapi")}) {
            Class[] types = new Class[]{
                java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.btnAñadirFichRapi.setText(p.getProperty("btnAñadirFichRapi"));
        Main.btnEliminarRapi.setText(p.getProperty("btnEliminarRapi"));
        Main.lblNombreCopiaLRapi.setText(p.getProperty("lblNombreCopiaLRapi"));
        Main.lblRutaDestinoRapi.setText(p.getProperty("lblRutaDestinoRapi"));
        Main.btnCPreferidaRapi.setText(p.getProperty("btnCPreferidaRapi"));
        Main.btnCOtraRapi.setText(p.getProperty("btnCOtraRapi"));
        Main.lblInfRapi.setText(p.getProperty("lblInfRapi"));
        Main.btnCopiaContraseñaRapi.setText(p.getProperty("btnCopiaContraseñaRapi"));
        Main.btnHacerCopiaRapi.setText(p.getProperty("btnHacerCopiaRapi"));
        Main.btnRestablecerRapi.setText(p.getProperty("btnRestablecerRapi"));
        Main.btnCancelarRapi.setText(p.getProperty("btnCancelarRapi"));
        Main.btnAñadirCopia.setText(p.getProperty("btnAñadirCopia"));
        Main.btnRestaurarCopiaLocal.setText(p.getProperty("btnRestaurarCopiaLocal"));
        Main.btnHacerSeleccionadasLocal.setText(p.getProperty("btnHacerSeleccionadasLocal"));
        Main.btnEliminarCopiaLocal.setText(p.getProperty("btnEliminarCopiaLocal"));
        Main.tblRegistroLocal.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("lblNombreGesLocales"), p.getProperty("lblUbicacionGesLocales"), p.getProperty("lblTamañoGesLocales"), p.getProperty("lblFechaGesLocales"), p.getProperty("lblHoraGesLocales"), p.getProperty("lblDiasGesLocales"), p.getProperty("lblDiasGesLocales").equals("Días desde la última copia") ? "Hacer copia" : "Make copy"}) {
            Class[] types = new Class[]{
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, true
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.btnAgregarCopiaLinea.setText(p.getProperty("btnAgregarCopiaLinea"));
        Main.btnRestaurarLinea.setText(p.getProperty("btnRestaurarLinea"));
        Main.btnEliminarCopiaLinea.setText(p.getProperty("btnEliminarCopiaLinea"));
        Main.tblRegistroLinea.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("lblNombreGestionLin"), p.getProperty("lblTamañoGestionLin"), p.getProperty("lblFechaGestionLin"), p.getProperty("lblHoraGestionLin")}) {
            Class[] types = new Class[]{
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.tblAchadirEL.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("lblFicherosAnadLin")}) {
            Class[] types = new Class[]{
                java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.btnAñadirFichEL.setText(p.getProperty("btnAñadirFichEL"));
        Main.btnEliminarEL.setText(p.getProperty("btnEliminarEL"));
        Main.lblNombreCopiaEL.setText(p.getProperty("lblNombreCopiaEL"));
        Main.lblInfEL.setText(p.getProperty("lblInfEL"));
        Main.btnCopiaPasswEL.setText(p.getProperty("btnCopiaPasswEL"));
        Main.btnGuardarEL.setText(p.getProperty("btnGuardarEL"));
        Main.btnRestablecerEL.setText(p.getProperty("btnRestablecerEL"));
        Main.btnCancelarEL.setText(p.getProperty("btnCancelarEL"));
        Main.tblAchadirL.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("lblFicherosRegisLocal")}) {
            Class[] types = new Class[]{
                java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.btnAñadirFichL.setText(p.getProperty("btnAñadirFichL"));
        Main.btnEliminarL.setText(p.getProperty("btnEliminarL"));
        Main.lblNombreCopiaL.setText(p.getProperty("lblNombreCopiaL"));
        Main.lblRutaDestinoL.setText(p.getProperty("lblRutaDestinoL"));
        Main.btnCPreferidaL.setText(p.getProperty("btnCPreferidaL"));
        Main.btnCOtraL.setText(p.getProperty("btnCOtraL"));
        Main.lblInfL.setText(p.getProperty("lblInfL"));
        Main.btnHacerGuardarL.setText(p.getProperty("btnHacerGuardarL"));
        Main.btnCopiaContraseñaL.setText(p.getProperty("btnCopiaContraseñaL"));
        Main.btnGuardarL.setText(p.getProperty("btnGuardarL"));
        Main.btnRestablecerL.setText(p.getProperty("btnRestablecerL"));
        Main.btnCancelarL.setText(p.getProperty("btnCancelarL"));
        Main.lblRestaurarCS.setText(p.getProperty("lblRestaurarCS"));
        Main.lblGuardadaEn.setText(p.getProperty("lblGuardadaEn"));
        Main.lblDondeRes.setText(p.getProperty("lblDondeRes"));
        Main.btnRest.setText(p.getProperty("btnRest"));
        Main.btnCancelarRestaurar.setText(p.getProperty("btnCancelarRestaurar"));
        Main.tblOrigen.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("lblSelecDisOri")}) {
            boolean[] canEdit = new boolean[]{
                false
            };
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.tblDestino.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("lblSelecDisDest")}) {
            Class[] types = new Class[]{
                java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.lblNombrepara.setText(p.getProperty("lblNombrepara"));
        Main.btnClonado.setText(p.getProperty("btnClonado"));
        Main.btnCancelarClonado.setText(p.getProperty("btnCancelarClonado"));
        Main.lblContactoNombre.setText(p.getProperty("lblContactoNombre"));
        Main.lblContactoApellidos.setText(p.getProperty("lblContactoApellidos"));
        Main.lblContactoCorreo.setText(p.getProperty("lblContactoCorreo"));
        Main.lblContactoAsunto.setText(p.getProperty("lblContactoAsunto"));
        Main.lblContactoMensaje.setText(p.getProperty("lblContactoMensaje"));
        Main.btnEnviarCorreo.setText(p.getProperty("btnEnviarCorreo"));
        Main.btnContactoRestablecer.setText(p.getProperty("btnContactoRestablecer"));
        Main.btnCancelarMail.setText(p.getProperty("btnCancelarMail"));
        Metodos.cargaUnidades(Main.tblUnidades);
        tblModificar.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("tblModificar")}) {
            Class[] types = new Class[]{
                java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.btnAnadirFichModificar.setText(p.getProperty("btnAnadirFichModificar"));
        Main.btnModificarRegistroEli.setText(p.getProperty("btnModificarRegistroEli"));
        Main.lblNombreParaTuCM.setText(p.getProperty("lblNombreParaTuCM"));
        Main.lblGuardadaM.setText(p.getProperty("lblGuardadaM"));
        Main.lblInfMod.setText(p.getProperty("lblInfMod"));
        Main.btnHacerCopiaMOD.setText(p.getProperty("btnHacerCopiaMOD"));
        Main.btnCopiaContraMOD.setText(p.getProperty("btnCopiaContraMOD"));
        Main.btnGuardarMOD.setText(p.getProperty("btnGuardarMOD"));
        Main.btnCancelarMod.setText(p.getProperty("btnCancelarMod"));
        holder = new PlaceHolder(Main.txtUsuarioLogin, Color.gray, Color.black, p.getProperty("logCorreo"), false, getFontPrincipal(), getTamañoPricipal());
        holder = new PlaceHolder(Main.txtContraseñaLogin, Color.gray, Color.black, p.getProperty("logClave"), false, getFontPrincipal(), getTamañoPricipal());
        Main.txtContraseñaLogin.setEchoChar((char) 0);
        Main.panelMiCuenta.setBorder(javax.swing.BorderFactory.createTitledBorder(null, p.getProperty("panelMiCuenta"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Microsoft JhengHei", 0, 18))); // NOI18N
        Main.panelLenguaje.setBorder(javax.swing.BorderFactory.createTitledBorder(null, p.getProperty("panelLenguaje"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Microsoft JhengHei", 0, 18))); // NOI18N
        Main.panelAspecto.setBorder(javax.swing.BorderFactory.createTitledBorder(null, p.getProperty("panelAspecto"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Microsoft JhengHei", 0, 18))); // NOI18N
        Main.panelAvanzado.setBorder(javax.swing.BorderFactory.createTitledBorder(null, p.getProperty("panelAvanzado"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Microsoft JhengHei", 0, 18))); // NOI18N
        Main.btnCerrarSes.setText(p.getProperty("btnCerrarSes"));
        Main.btnMisDatos.setText(p.getProperty("btnMisDatos"));
        Main.btnCambiarIdioma.setText(p.getProperty("btnCambiarIdioma"));
        Main.btnRestablecerTraduc.setText(p.getProperty("btnRestablecerTraduc"));
        Main.btnCambiarTema.setText(p.getProperty("btnCambiarTema"));
        Main.btnRestablecerTemaPrede.setText(p.getProperty("btnRestablecerTemaPrede"));
        Main.btnAvisoDiasCopia.setText(p.getProperty("btnAvisoDiasCopia"));
        Main.btnComprobarActualizaciones1.setText(p.getProperty("btnComprobarActualizaciones1"));
        Main.btnCambiarRutaGuardado.setText(p.getProperty("btnCambiarRutaGuardado"));
        Main.btnComprobarActualizaciones.setText(p.getProperty("btnComprobarActualizaciones"));
        Main.btnEnviarDatosDiagnos.setText(p.getProperty("btnEnviarDatosDiagnos"));
        Main.btnRestaurarDatosPredeter.setText(p.getProperty("btnRestaurarDatosPredeter"));
        Main.btnSobreSawUp.setText(p.getProperty("btnSobreSawUp"));
        Main.lblCnfNombre.setText(p.getProperty("lblCnfNombre"));
        Main.lblCnfApe.setText(p.getProperty("lblCnfApe"));
        Main.btnConfHabNom.setText(p.getProperty("btnConfHabNom"));
        Main.lblCnfContra.setText(p.getProperty("lblCnfContra"));
        Main.btnConfHabilPass.setText(p.getProperty("btnConfHabilPass"));
        Main.btnCambInfo.setText(p.getProperty("btnCambInfo"));
        Main.btnRestCampoConfDat.setText(p.getProperty("btnRestCampoConfDat"));
        Main.lblTemaActual.setText(p.getProperty("lblTemaActual"));
        Main.tblTemasDisp.setModel(new javax.swing.table.DefaultTableModel(new Object[][]{}, new String[]{p.getProperty("tblTemasDisp")}) {
            Class[] types = new Class[]{
                java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false
            };
            
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
            
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        Main.btnAplicarTema.setText(p.getProperty("btnAplicarTema"));
        Main.btnNoti.setText(p.getProperty("btnNoti"));
        Main.btnClaras.setText(p.getProperty("btnClaras"));
        Main.btnOscuras.setText(p.getProperty("btnOscuras"));
        Main.btnAplicarIdioma.setText(p.getProperty("btnAplicarIdioma"));
        Main.lblInfoPanel.setText(p.getProperty("lblInfoPanel"));
        Main.btnRestartPassword.setText(p.getProperty("btnRestartPassword"));
    }
    
    public static Properties comprobarLenguaje(Properties p) {
        try {
            p.load(new FileReader(Paths.getSTARTF()));
            switch (p.getProperty("language")) {
                case "spanish":
                    p.load(new FileReader(Paths.getESPF()));
                    break;
                case "english":
                    p.load(new FileReader(Paths.getENGF()));
                    break;
                default:
                    p.load(new FileReader(Paths.getESPF()));
                    break;
            }
        } catch (FileNotFoundException ex) {
            new RegistroEnvioError(5, ex.getMessage()).start();
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        return p;
    }
    
    public static void traduccion(String lan) {
        try {
            Properties p = new Properties();
            if (lan.equals("spanish")) {
                p.load(new FileReader(Paths.getESPF()));
                p.setProperty("lblRestarPass", "Introduce tu correo electrónico:");
                p.setProperty("logCorreo", "Correo electrónico");
                p.setProperty("logClave", "Contraseña");
                p.setProperty("btnInciarSesion", "Iniciar sesión");
                p.setProperty("btnRegistrar", "Registrarse");
                p.setProperty("btnRestartPass", "Recuperar contraseña");
                p.setProperty("btnCopiaLocal", "Copias locales");
                p.setProperty("btnCopiaEnLinea", "Copias en linea");
                p.setProperty("btnCopiaRapida", "Copia rapida");
                p.setProperty("btnClonar", "Clonar unidad");
                p.setProperty("btnContacto", "Contacto");
                p.setProperty("btnConfiguracion", "Configuración");
                p.setProperty("lblMisUnidades", "Mis unidades de almacenamiento");
                p.setProperty("lblUniResumen", "Dispositivo");
                p.setProperty("lblEspaDispResumen", "Espacio disponible");
                p.setProperty("lblEspaTotaResu", "Espacio total");
                p.setProperty("btnFormatear", "Formatear");
                p.setProperty("btnAbrir", "Abrir ubicación");
                p.setProperty("btnCorregir", "Corregir errores");
                p.setProperty("lblTareas", "Tareas que se estan realizando: 0");
                p.setProperty("lblIdentifiResumen", "Identificador");
                p.setProperty("lblDescrResumen", "Descripción");
                p.setProperty("lblTablFichRapi", "Ficheros que se incluyen en la copia:");
                p.setProperty("btnAñadirFichRapi", "Añadir archivo");
                p.setProperty("btnEliminarRapi", "Eliminar registro");
                p.setProperty("lblNombreCopiaLRapi", "Nombre para tu copia:");
                p.setProperty("lblRutaDestinoRapi", "¿Donde se guardara la copia de seguridad?");
                p.setProperty("btnCPreferidaRapi", "Ubicación preferida");
                p.setProperty("btnCOtraRapi", "Otra Ubicacion");
                p.setProperty("lblInfRapi", "Información sobre la copia de seguridad:");
                p.setProperty("btnCopiaContraseñaRapi", "Copia de seguridad con contraseña");
                p.setProperty("btnHacerCopiaRapi", "Hacer copia");
                p.setProperty("btnRestablecerRapi", "Restablecer campos");
                p.setProperty("btnCancelarRapi", "Cancelar");
                p.setProperty("btnAñadirCopia", "Nueva copia");
                p.setProperty("btnRestaurarCopiaLocal", "Restaurar");
                p.setProperty("btnHacerSeleccionadasLocal", "Hacer copias seleccionadas");
                p.setProperty("btnEliminarCopiaLocal", "Eliminar");
                p.setProperty("btnEditarCopia", "Editar");
                p.setProperty("lblNombreGesLocales", "Nombre");
                p.setProperty("lblUbicacionGesLocales", "Ubicación");
                p.setProperty("lblTamañoGesLocales", "Tamaño");
                p.setProperty("lblFechaGesLocales", "Fecha de creación");
                p.setProperty("lblHoraGesLocales", "Hora de creación");
                p.setProperty("lblDiasGesLocales", "Días desde la última copia");
                p.setProperty("btnAgregarCopiaLinea", "Crear nueva copia");
                p.setProperty("btnRestaurarLinea", "Restaurar");
                p.setProperty("btnEliminarCopiaLinea", "Eliminar una copia");
                p.setProperty("lblNombreGestionLin", "Nombre");
                p.setProperty("lblTamañoGestionLin", "Tamaño");
                p.setProperty("lblFechaGestionLin", "Fecha de creación");
                p.setProperty("lblHoraGestionLin", "Hora de creación");
                p.setProperty("lblFicherosAnadLin", "Ficheros que se incluyen en la copia");
                p.setProperty("btnAñadirFichEL", "Añadir archivo");
                p.setProperty("btnEliminarEL", "Eliminar registro");
                p.setProperty("lblNombreCopiaEL", "Nombre para tu copia:");
                p.setProperty("lblInfEL", "Información sobre la copia de seguridad:");
                p.setProperty("btnCopiaPasswEL", "Copia de seguridad con contraseña");
                p.setProperty("btnGuardarEL", "Guardar cambios");
                p.setProperty("btnRestablecerEL", "Restablecer campos");
                p.setProperty("btnCancelarEL", "Cancelar");
                p.setProperty("lblFicherosRegisLocal", "Ficheros que se incluyen en la copia");
                p.setProperty("btnAñadirFichL", "Añadir archivo");
                p.setProperty("btnEliminarL", "Eliminar archivo");
                p.setProperty("lblNombreCopiaL", "Nombre para tu copia:");
                p.setProperty("lblRutaDestinoL", "¿Dónde se guardará la copia de seguridad?");
                p.setProperty("btnCPreferidaL", "Ubicación preferida");
                p.setProperty("btnCOtraL", "Otra ubicacion");
                p.setProperty("lblInfL", "Información sobre la copia de seguridad:");
                p.setProperty("btnHacerGuardarL", "Hacer copia al guardar los cambios");
                p.setProperty("btnCopiaContraseñaL", "Copia de seguridad con contraseña");
                p.setProperty("btnGuardarL", "Guardar cambios");
                p.setProperty("btnRestablecerL", "Restablecer campos");
                p.setProperty("btnCancelarL", "Cancelar");
                p.setProperty("lblRestaurarCS", "Copia de seguridad:");
                p.setProperty("lblGuardadaEn", "Guardada en:");
                p.setProperty("lblDondeRes", "¿Donde restaurar?");
                p.setProperty("btnRest", "Restaurar");
                p.setProperty("btnCancelarRestaurar", "Cancelar");
                p.setProperty("lblSelecDisOri", "Seleccione el dispositivo de origen");
                p.setProperty("lblSelecDisDest", "Seleccione el dispositivo de destino");
                p.setProperty("lblNombrepara", "Nombre para tu dispositivo clonado:");
                p.setProperty("btnClonado", "Clonar");
                p.setProperty("btnCancelarClonado", "Cancelar");
                p.setProperty("lblContactoNombre", "Nombre:");
                p.setProperty("lblContactoApellidos", "Apellidos:");
                p.setProperty("lblContactoCorreo", "Correo electronico:");
                p.setProperty("lblContactoAsunto", "Asunto:");
                p.setProperty("lblContactoMensaje", "Mensaje:");
                p.setProperty("btnEnviarCorreo", "Enviar");
                p.setProperty("btnContactoRestablecer", "Restablecer campos");
                p.setProperty("btnCancelarMail", "Cancelar");
                p.setProperty("tblModificar", "Ficheros que se incluyen en la copia.");
                p.setProperty("btnAnadirFichModificar", "Añadir archivo");
                p.setProperty("btnModificarRegistroEli", "Eliminar registro");
                p.setProperty("lblNombreParaTuCM", "Nombre de tu copia:");
                p.setProperty("lblGuardadaM", "Guardada en:");
                p.setProperty("lblInfMod", "Información sobre la copia de seguridad:");
                p.setProperty("btnHacerCopiaMOD", "Hacer copia al guardar los cambios");
                p.setProperty("btnCopiaContraMOD", "Copia de seguridad con contraseña");
                p.setProperty("btnGuardarMOD", "Guardar cambios");
                p.setProperty("btnCancelarMod", "Cancelar");
                p.setProperty("panelMiCuenta", "Mi cuenta");
                p.setProperty("panelLenguaje", "Lenguaje");
                p.setProperty("panelAspecto", "Aspecto");
                p.setProperty("panelAvanzado", "Avanzado");
                p.setProperty("btnCerrarSes", "Cerrar sesión");
                p.setProperty("btnMisDatos", "Mis datos");
                p.setProperty("btnCambiarIdioma", "Cambiar idioma");
                p.setProperty("btnRestablecerTraduc", "Restablecer traducciones");
                p.setProperty("btnCambiarTema", "Cambiar de tema");
                p.setProperty("btnRestablecerTemaPrede", "Restablecer tema predeterminado");
                p.setProperty("btnAvisoDiasCopia", "Aviso desde la última copia");
                p.setProperty("btnComprobarActualizaciones1", "Busqueda de actualizaciones al inicio");
                p.setProperty("btnCambiarRutaGuardado", "Cambiar ruta de guardado");
                p.setProperty("btnComprobarActualizaciones", "Comprobar actualizaciones");
                p.setProperty("btnEnviarDatosDiagnos", "Envió de datos de diagnostico");
                p.setProperty("btnRestaurarDatosPredeter", "Restaurar datos predeterminados");
                p.setProperty("btnSobreSawUp", "Sobre Saw Up");
                p.setProperty("lblCnfNombre", "Nombre:");
                p.setProperty("lblCnfApe", "Apellidos:");
                p.setProperty("btnConfHabNom", "Habilitar el cambio de nombre y apellidos");
                p.setProperty("lblCnfContra", "Contraseña:");
                p.setProperty("btnConfHabilPass", "Habilitar el cambio de contraseña");
                p.setProperty("btnCambInfo", "Cambiar mi informacion");
                p.setProperty("btnRestCampoConfDat", "Restablecer campos");
                p.setProperty("lblTemaActual", "Actualmente se esta aplicando el tema:");
                p.setProperty("tblTemasDisp", "Temas disponibles");
                p.setProperty("btnAplicarTema", "Aplicar el tema seleccionado");
                p.setProperty("btnNoti", "Color de notificaciones:");
                p.setProperty("btnClaras", "Claras");
                p.setProperty("btnOscuras", "Oscuras");
                p.setProperty("btnAplicarIdioma", "Aplicar cambios: Español");
                p.setProperty("lblInfoPanel", "Configuración de la aplicación");
                p.setProperty("lblTipoFormat", "Seleccine un tipo de formateo:");
                p.setProperty("btnFormateoRapido", "Rápido");
                p.setProperty("btnFormateoCompleto", "Completo");
                p.setProperty("lblTipoParticion", "Seleccione un tipo de partición:");
                p.setProperty("btnHacerFormateo", "Formatear");
                p.setProperty("btnRestartPassword", "Restaurar contraseña");
                p.store(new BufferedWriter(new FileWriter(Paths.getESPF())), "Spanish File");
            } else if (lan.equals("english")) {
                p.load(new FileReader(Paths.getENGF()));
                p.setProperty("lblRestarPass", "Enter your email:");
                p.setProperty("logCorreo", "Email");
                p.setProperty("logClave", "Password");
                p.setProperty("btnInciarSesion", "Log in");
                p.setProperty("btnRegistrar", "Check in");
                p.setProperty("btnRestartPass", "Recover password");
                p.setProperty("btnCopiaLocal", "Local copies");
                p.setProperty("btnCopiaEnLinea", "Online copies");
                p.setProperty("btnCopiaRapida", "Quick copy");
                p.setProperty("btnClonar", "Clone unit");
                p.setProperty("btnContacto", "Contact");
                p.setProperty("btnConfiguracion", "Setting");
                p.setProperty("lblMisUnidades", "My storage drives");
                p.setProperty("lblUniResumen", "Device");
                p.setProperty("lblEspaDispResumen", "Available space");
                p.setProperty("lblEspaTotaResu", "Total space");
                p.setProperty("btnFormatear", "Format");
                p.setProperty("btnAbrir", "Open location");
                p.setProperty("btnCorregir", "Fix errors");
                p.setProperty("lblTareas", "Tasks in progress: 0");
                p.setProperty("lblIdentifiResumen", "Identifier");
                p.setProperty("lblDescrResumen", "Description");
                p.setProperty("lblTablFichRapi", "Files included in the copy:");
                p.setProperty("btnAñadirFichRapi", "Add file");
                p.setProperty("btnEliminarRapi", "Delete file");
                p.setProperty("lblNombreCopiaLRapi", "Name for your copy:");
                p.setProperty("lblRutaDestinoRapi", "Where will the backup be saved?");
                p.setProperty("btnCPreferidaRapi", "Preferred location");
                p.setProperty("btnCOtraRapi", "Another location");
                p.setProperty("lblInfRapi", "Backup information:");
                p.setProperty("btnCopiaContraseñaRapi", "Password backup");
                p.setProperty("btnHacerCopiaRapi", "Make copy");
                p.setProperty("btnRestablecerRapi", "Reset fields");
                p.setProperty("btnCancelarRapi", "Cancel");
                p.setProperty("btnAñadirCopia", "Add new copy");
                p.setProperty("btnEditarCopia", "Edit");
                p.setProperty("btnRestaurarCopiaLocal", "Restore a copy");
                p.setProperty("btnHacerSeleccionadasLocal", "Make selected copies");
                p.setProperty("btnEliminarCopiaLocal", "Delete a copy");
                p.setProperty("lblNombreGesLocales", "Name");
                p.setProperty("lblUbicacionGesLocales", "Location");
                p.setProperty("lblTamañoGesLocales", "Size");
                p.setProperty("lblFechaGesLocales", "Creation date");
                p.setProperty("lblHoraGesLocales", "Creation time");
                p.setProperty("lblDiasGesLocales", "Days since last copy");
                p.setProperty("btnAgregarCopiaLinea", "Create new copy");
                p.setProperty("btnRestaurarLinea", "Restore");
                p.setProperty("btnEliminarCopiaLinea", "Delete a copy");
                p.setProperty("lblNombreGestionLin", "Name");
                p.setProperty("lblTamañoGestionLin", "Size");
                p.setProperty("lblFechaGestionLin", "Creation date");
                p.setProperty("lblHoraGestionLin", "Creation time");
                p.setProperty("lblFicherosAnadLin", "Files included in the copy");
                p.setProperty("btnAñadirFichEL", "Add file");
                p.setProperty("btnEliminarEL", "Delete record");
                p.setProperty("lblNombreCopiaEL", "Name for your copy:");
                p.setProperty("lblInfEL", "Backup information:");
                p.setProperty("btnCopiaPasswEL", "Password backup");
                p.setProperty("btnGuardarEL", "Save changes");
                p.setProperty("btnRestablecerEL", "Reset fields");
                p.setProperty("btnCancelarEL", "Cancel");
                p.setProperty("lblFicherosRegisLocal", "Files included in the copy");
                p.setProperty("btnAñadirFichL", "Add file");
                p.setProperty("btnEliminarL", "Delete file");
                p.setProperty("lblNombreCopiaL", "Name for your copy:");
                p.setProperty("lblRutaDestinoL", "Where will the backup be saved?");
                p.setProperty("btnCPreferidaL", "Preferred location");
                p.setProperty("btnCOtraL", "Another location");
                p.setProperty("lblInfL", "Backup information:");
                p.setProperty("btnHacerGuardarL", "Make a copy when saving changes");
                p.setProperty("btnCopiaContraseñaL", "Password backup");
                p.setProperty("btnGuardarL", "Save changes");
                p.setProperty("btnRestablecerL", "Reset fields");
                p.setProperty("btnCancelarL", "Cancel");
                p.setProperty("lblRestaurarCS", "Backup:");
                p.setProperty("lblGuardadaEn", "Filed in:");
                p.setProperty("lblDondeRes", "Where to restore?");
                p.setProperty("btnRest", "Restore");
                p.setProperty("btnCancelarRestaurar", "Cancel");
                p.setProperty("lblSelecDisOri", "Select the source device");
                p.setProperty("lblSelecDisDest", "Select the target device");
                p.setProperty("lblNombrepara", "Name for your cloned device:");
                p.setProperty("btnClonado", "Clone");
                p.setProperty("btnCancelarClonado", "Cancel");
                p.setProperty("lblContactoNombre", "Name:");
                p.setProperty("lblContactoApellidos", "Surnames:");
                p.setProperty("lblContactoCorreo", "Email:");
                p.setProperty("lblContactoAsunto", "Subject:");
                p.setProperty("lblContactoMensaje", "Message:");
                p.setProperty("btnEnviarCorreo", "Send");
                p.setProperty("btnContactoRestablecer", "Reset fields");
                p.setProperty("btnCancelarMail", "Cancel");
                p.setProperty("tblModificar", "Files that are included in the copy.");
                p.setProperty("btnAnadirFichModificar", "Add file");
                p.setProperty("btnModificarRegistroEli", "Delete record");
                p.setProperty("lblNombreParaTuCM", "Name of your copy:");
                p.setProperty("lblGuardadaM", "Filed in:");
                p.setProperty("lblInfMod", "Backup information:");
                p.setProperty("btnHacerCopiaMOD", "Make a copy when saving changes");
                p.setProperty("btnCopiaContraMOD", "Password backup");
                p.setProperty("btnGuardarMOD", "Save changes");
                p.setProperty("btnCancelarMod", "Cancel");
                p.setProperty("panelMiCuenta", "My account");
                p.setProperty("panelLenguaje", "Language");
                p.setProperty("panelAspecto", "Appearance");
                p.setProperty("panelAvanzado", "Advanced");
                p.setProperty("btnCerrarSes", "Sign off");
                p.setProperty("btnMisDatos", "My data");
                p.setProperty("btnCambiarIdioma", "Change language");
                p.setProperty("btnRestablecerTraduc", "Reset translations");
                p.setProperty("btnCambiarTema", "Change theme");
                p.setProperty("btnRestablecerTemaPrede", "Reset default theme");
                p.setProperty("btnAvisoDiasCopia", "Notice since last copy");
                p.setProperty("btnComprobarActualizaciones1", "Check for updates at startup");
                p.setProperty("btnCambiarRutaGuardado", "Change save path");
                p.setProperty("btnComprobarActualizaciones", "Check for updates");
                p.setProperty("btnEnviarDatosDiagnos", "Sending of diagnostic data");
                p.setProperty("btnRestaurarDatosPredeter", "Restore default data");
                p.setProperty("btnSobreSawUp", "About Saw Up");
                p.setProperty("lblCnfNombre", "Name:");
                p.setProperty("lblCnfApe", "Surnames:");
                p.setProperty("btnConfHabNom", "Enable the change of name and surname");
                p.setProperty("lblCnfContra", "Password:");
                p.setProperty("btnConfHabilPass", "Enable password change");
                p.setProperty("btnCambInfo", "Change my information");
                p.setProperty("btnRestCampoConfDat", "Reset fields");
                p.setProperty("lblTemaActual", "The theme is currently being applied:");
                p.setProperty("tblTemasDisp", "Available themes");
                p.setProperty("btnAplicarTema", "Apply the selected theme");
                p.setProperty("btnNoti", "Notifications color:");
                p.setProperty("btnClaras", "Clear");
                p.setProperty("btnOscuras", "Dark");
                p.setProperty("btnAplicarIdioma", "Apply changes: English");
                p.setProperty("lblInfoPanel", "Application settings");
                p.setProperty("lblTipoFormat", "Select a format type:");
                p.setProperty("btnFormateoRapido", "Quick");
                p.setProperty("btnFormateoCompleto", "Full");
                p.setProperty("lblTipoParticion", "Select a partition type:");
                p.setProperty("btnHacerFormateo", "Format");
                p.setProperty("btnRestartPassword", "Restart password");
                p.store(new BufferedWriter(new FileWriter(Paths.getENGF())), "English File");
            }
        } catch (FileNotFoundException ex) {
            new RegistroEnvioError(5, ex.getMessage()).start();
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
    }
    
    public static String getFontPrincipal() {
        return fontPrincipal;
    }
    
    public static int getTamañoPricipal() {
        return tamañoPricipal;
    }
    
}
