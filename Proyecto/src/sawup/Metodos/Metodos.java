package sawup.Metodos;

import com.db4o.*;
import com.jcraft.jsch.*;
import com.mysql.jdbc.PacketTooBigException;
import ds.desktop.notify.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.security.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.logging.Level;
import java.util.regex.*;
import javax.swing.*;
import javax.swing.table.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.jvnet.substance.SubstanceLookAndFeel;
import sawup.Clases.*;
import sawup.*;
import sawup.Hilos.DetectorUSB;
import sawup.Hilos.Mail;
import sawup.Hilos.RegistroEnvioError;
import static sawup.Main.*;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Metodos {

    public static String[] themes = {"BusinessBlackSteelSkin", "BusinessSkin", "CremeSkin", "MistAquaSkin", "MistSilverSkin", "NebulaBrickWallSkin", "RavenGraphiteGlassSkin", ""};

    public static void setLook() throws IOException {
        try {
            JFrame.setDefaultLookAndFeelDecorated(false);
            Properties p = new Properties();
            p.load(new FileReader(Paths.getSTARTF()));
            if (p.getProperty("theme").equals(themes[0]) || p.getProperty("theme").equals(themes[1]) || p.getProperty("theme").equals(themes[2]) || p.getProperty("theme").equals(themes[3]) || p.getProperty("theme").equals(themes[4]) || p.getProperty("theme").equals(themes[5]) || p.getProperty("theme").equals(themes[6])) {
                themes[7] = p.getProperty("theme");
            } else {
                themes[7] = "MistAquaSkin";
            }
            SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin." + themes[7]);
            themes[7] = "";
            p.store(new BufferedWriter(new FileWriter(Paths.getSTARTF())), "Start File");
            switch (p.getProperty("not")) {
                case "light":
                    DesktopNotify.setDefaultTheme(NotifyTheme.Light);
                    break;
                case "dark":
                    DesktopNotify.setDefaultTheme(NotifyTheme.Dark);
                    break;
                default:
                    DesktopNotify.setDefaultTheme(NotifyTheme.Light);
                    break;
            }
            UIManager.put("OptionPane.messageFont", new Font("Microsoft JhengHei", Font.PLAIN, 14));
            UIManager.put("OptionPane.buttonFont", new Font("Microsoft JhengHei", Font.PLAIN, 14));
        } catch (FileNotFoundException ex) {
            new RegistroEnvioError(5, ex.getMessage()).start();
        }
    }

    public static boolean emailTest(String email) {
        Pattern pat = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");
        Matcher mat = pat.matcher(email);
        if (mat.find() == true) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean passwordTest(String password) {
        Pattern pat = Pattern.compile("^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{8,16}$");
        Matcher mat = pat.matcher(password);
        if (mat.find() == true) {
            return true;
        } else {
            return false;
        }
    }

    public static void setAjuste(String rutaFichero, String propiedad, String valor, String comentario) throws FileNotFoundException, IOException {
        Properties p = new Properties();
        p.load(new FileReader(rutaFichero));
        p.setProperty(propiedad, valor);
        p.store(new BufferedWriter(new FileWriter(rutaFichero)), comentario);
    }

    public static String getAjuste(String ruta, String propiedad) throws FileNotFoundException, IOException {
        Properties p = new Properties();
        p.load(new FileReader(ruta));
        return p.getProperty(propiedad);
    }

    public static void connect(Connection conn, String str1, String str2, int op, CardLayout layout) throws SQLException, IOException, InterruptedException {
        PreparedStatement stmt = null;
        try {
            if (Main.session == null) {
                Main.session = Main.jSch.getSession(Metodos.getAjuste(Paths.getCONP(), "ssh"), Paths.getIP(), 22);
                Main.session.setConfig("StrictHostKeyChecking", "no");
                Main.session.setPassword(Metodos.getAjuste(Paths.getCONP(), "ssh2"));
                Main.session.connect();
                Main.session.setPortForwardingL(3306, Paths.getIP(), 3306);
            }
            if (conn == null) {
                conn = new Conexion().conexionMySQL();
            }
            if (Main.session.isConnected() && conn != null) {
                switch (op) {
                    case 1:
                        try {
                            stmt = conn.prepareStatement(Consultas.getSELECCIONAR_USUARIO());
                            stmt.setString(1, str1);
                            ResultSet rs = stmt.executeQuery();
                            if (rs.first()) {
                                if (DigestUtils.sha512Hex(str2).equals(rs.getString(5))) {
                                    if (new File(Paths.getUSERDAT()).exists()) {
                                        new File(Paths.getUSERDAT()).delete();
                                    }
                                    cardLayout.show(slider, "cardResumen");
                                    new DetectorUSB().start();
                                    btnMenuPrincipal.setVisible(true);
                                    ObjectContainer bd = Db4oEmbedded.openFile(Paths.getUSERDAT());
                                    bd.store(new Usuario(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
                                    bd.close();
                                    setAjuste(Paths.getSTARTF(), "login", "on", "Start File");
                                    stmt = conn.prepareStatement(Consultas.getREGISTRO_INICIO());
                                    stmt.setInt(1, rs.getInt(1));
                                    stmt.setString(2, hora());
                                    stmt.setString(3, getPublicIP());
                                    stmt.setString(4, getNetbiosName());
                                    stmt.setString(5, System.getProperty("os.version"));
                                    stmt.setString(6, System.getProperty("user.name"));
                                    stmt.execute();
                                    txtUsuarioLogin.setText("");
                                    txtContraseñaLogin.setText("");
                                } else {
                                    JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "La contraseña introducida no es válida." : "The password is incorrect.", "Saw Up", JOptionPane.YES_OPTION, alertaErroneaIco);
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "El correo electrónico o la contraseña no son válidos." : "The email or password is invalid.", "Saw Up", JOptionPane.YES_OPTION, alertaErroneaIco);
                            }
                        } catch (IOException ex) {
                            new RegistroEnvioError(2, ex.getMessage()).start();
                        } catch (SQLException ex) {
                            new RegistroEnvioError(1, ex.getMessage()).start();
                        }
                        break;
                    case 2:
                        stmt = conn.prepareStatement(Consultas.getCAMBIO_CLAVE());
                        stmt.setString(1, str1);
                        stmt.setString(2, Usuario.user()[1]);
                        stmt.executeUpdate();
                        JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Su contraseña ha sido cambiada con éxito." : "Your password has been successfully changed.", "Saw Up", JOptionPane.YES_OPTION, Main.ok);
                        new Mail(1, Usuario.user()[1]).start();
                        break;
                    case 3:
                        FileInputStream fis = null;
                        try {
                            stmt = conn.prepareStatement(Consultas.getINSERTAR_COPIA());
                            File archivo = new File(str1);
                            fis = new FileInputStream(archivo);
                            stmt.setString(1, Usuario.user()[0]);
                            stmt.setString(2, archivo.getName());
                            stmt.setBinaryStream(3, fis, (int) archivo.length());
                            stmt.setString(4, hora());
                            stmt.setString(5, obtenerMD5ComoString(str1));
                            Main.modelo = (DefaultTableModel) Main.tblRegistroLinea.getModel();
                            Main.modelo.addRow(new Object[]{archivo.getName(), tamaño(archivo.length()), fecha(), hora(), 0, false});
                            stmt.execute();
                        } catch (FileNotFoundException ex) {
                            new RegistroEnvioError(5, ex.getMessage()).start();
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, ex);
                        } finally {
                            stmt.close();
                            fis.close();
                        }
                        break;
                    case 4: {
                        stmt = conn.prepareStatement(Consultas.getCONTEO_COPIA());
                        stmt.setInt(1, Integer.valueOf(Usuario.user()[0]));
                        ResultSet rs = stmt.executeQuery();
                        if (rs.first()) {
                            if (rs.getInt(1) != tblRegistroLinea.getRowCount()) {
                                stmt = conn.prepareStatement(Consultas.getSELECTOR_COPIAS());
                                stmt.setInt(1, Integer.valueOf(Usuario.user()[0]));
                                rs = stmt.executeQuery();
                                DefaultTableModel m = (DefaultTableModel) tblRegistroLinea.getModel();
                                m.setRowCount(0);
                                Main.contLin = 0;
                                while (rs.next()) {
                                    m.addRow(new Object[]{rs.getString(3), tamaño(rs.getBlob(4).length()), rs.getDate(5), rs.getString(6)});
                                    Main.contLin++;
                                    Thread.sleep(150);
                                }
                            }
                        }
                        break;
                    }
                    case 5: {
                        stmt = conn.prepareStatement(Consultas.getCOMPROBAR_VERSION());
                        ResultSet rs = stmt.executeQuery();
                        if (rs.first()) {
                            Properties p = new Properties();
                            p.load(new FileReader(Paths.getSTARTF()));
                            if (p.getProperty("ver").equals(rs.getString(1))) {
                                JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Esta utilizando la ultima version de Saw Up." : "You are using the latest version of Saw Up.", "Saw Up", JOptionPane.OK_OPTION, Main.ok);
                            } else {
                                if (JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Hay una actualización disponible para Saw Up.\n¿Desea descargar e instalar la nueva actualización?" : "An update is available for Saw Up. \nWant to download and install the new update?", "Saw Up", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, Main.alertaErroneaIco)) {
                                    JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Se está descargando la última versión de Saw Up,\ncuando la descarga finalice, se abrirá el instalador\npara completar la actualización." : "The latest version of Saw Up is being downloaded,\nwhen the download completes the installer will\n open to complete the update.", "Saw Up", JOptionPane.OK_OPTION, Main.alertaErroneaIco);
                                    recuperarFicheroBD(conn, stmt, Paths.getTEMPOF(), rs.getString(2), 0);
                                } else {
                                    JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Continuara usando la versión actual de Saw Up,\nse recomienda que instale la ultima versión." : "You will continue to use the current version of Saw Up,\nit is recommended that you install the latest version.", "Saw Up", JOptionPane.OK_OPTION, Main.ok);
                                }
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Esta utilizando la ultima version de Saw Up." : "You are using the latest version of Saw Up.", "Saw Up", JOptionPane.OK_OPTION, Main.ok);
                        }
                        break;
                    }
                    case 6:
                        DefaultTableModel modelo = (DefaultTableModel) Main.tblRegistroLinea.getModel();
                        modelo.removeRow(Integer.parseInt(str2));
                        stmt = conn.prepareStatement(Consultas.getBORRAR_COPIA());
                        stmt.setString(1, str1);
                        stmt.setInt(2, Integer.valueOf(Usuario.user()[0]));
                        stmt.executeUpdate();
                        break;
                    case 7:
                        recuperarFicheroBD(conn, stmt, str2, str1, 1);
                        break;
                    case 8: {
                        stmt = conn.prepareStatement(Consultas.getCONTEO_USUARIO());
                        stmt.setString(1, str1);
                        ResultSet rs = stmt.executeQuery();
                        if (rs.first()) {
                            if (rs.getInt(1) == 1) {
                                new Mail(0, str2, str1).start();
                                JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Se ha enviado un correo a " + str1 + " con la nueva contraseña." : "An email has been sent to " + str1 + " with the new password.", "Saw Up", JOptionPane.YES_OPTION, ok);
                                stmt = conn.prepareStatement(Consultas.getCAMBIO_CLAVE());
                                stmt.setString(1, str2);
                                stmt.setString(2, str1);
                                stmt.executeUpdate();
                                cardLayout.show(slider, "cardLogin");
                            } else {
                                JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "No se ha encontrado una cuenta con ese correo." : "An account was not found with that email.", "Saw Up", JOptionPane.YES_OPTION, alertaErroneaIco);
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "No se ha encontrado una cuenta con ese correo." : "An account was not found with that email.", "Saw Up", JOptionPane.YES_OPTION, alertaErroneaIco);
                        }
                        break;
                    }
                    case 9:
                        stmt = conn.prepareStatement(Consultas.getINSERTAR_PROBLEMA());
                        stmt.setString(1, str1);
                        stmt.setString(2, str2);
                        stmt.setString(3, hora());
                        stmt.setInt(4, Integer.parseInt(Usuario.user()[0]));
                        stmt.execute();
                        break;
                    case 10:
                        stmt = conn.prepareStatement(Consultas.getCAMBIO_NOMBRE_APELLIDOS());
                        stmt.setString(1, str1);
                        stmt.setString(2, str2);
                        stmt.setString(3, Usuario.user()[1]);
                        stmt.execute();
                        String[] user = Usuario.user();
                        user[2] = str1;
                        user[3] = str2;
                        if (new File(Paths.getUSERDAT()).exists()) {
                            new File(Paths.getUSERDAT()).delete();
                        }
                        ObjectContainer bd = Db4oEmbedded.openFile(Paths.getUSERDAT());
                        bd.store(new Usuario(Integer.parseInt(user[0]), user[1], user[2], user[3]));
                        bd.close();
                        JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Se ha actualizado su nombre y apellidos." : "Your name and surname have been updated.", "Saw Up", JOptionPane.YES_OPTION, Main.ok);
                        break;
                    case 11: {
                        stmt = conn.prepareStatement(Consultas.getCOMPROBAR_VERSION());
                        ResultSet rs = stmt.executeQuery();
                        if (rs.first()) {
                            if (!getAjuste(Paths.getSTARTF(), "ver").equals(rs.getString(1))) {
                                if (JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Hay una actualización disponible para Saw Up.\n¿Desea descargar e instalar la nueva actualización?" : "An update is available for Saw Up. \nWant to download and install the new update?", "Saw Up", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, Main.alertaErroneaIco)) {
                                    JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Se está descargando la última versión de Saw Up,\ncuando la descarga finalice, se abrirá el instalador\npara completar la actualización." : "The latest version of Saw Up is being downloaded,\nwhen the download completes the installer will\n open to complete the update.", "Saw Up", JOptionPane.OK_OPTION, Main.alertaErroneaIco);
                                    setAjuste(Paths.getSTARTF(), "ver", rs.getString(1), "Start File");
                                    recuperarFicheroBD(conn, stmt, Paths.getTEMPOF(), rs.getString(2), 0);
                                } else {
                                    JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Continuara usando la versión actual de Saw Up,\nse recomienda que instale la ultima versión." : "You will continue to use the current version of Saw Up,\nit is recommended that you install the latest version.", "Saw Up", JOptionPane.OK_OPTION, Main.ok);
                                }
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "Esta utilizando la ultima version de Saw Up." : "You are using the latest version of Saw Up.", "Saw Up", JOptionPane.OK_OPTION, Main.ok);
                        }
                        break;
                    }
                    default:
                        break;
                }
            } else {
                JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "No se puede establecer comunicación con el servidor." : "Cannot communicate with the server.", "Saw Up", JOptionPane.OK_OPTION, Main.alertaErroneaIco);
            }
        } catch (JSchException ex) {
            JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "No se puede establecer comunicación con el servidor." : "Cannot communicate with the server.", "Saw Up", JOptionPane.YES_OPTION, alertaErroneaIco);
            new RegistroEnvioError(4, ex.getMessage()).start();
        } catch (UnknownHostException ex) {
            new RegistroEnvioError(15, ex.getMessage()).start();
        } catch (PacketTooBigException ex) {
            new RegistroEnvioError(18, ex.getMessage()).start();
        }
    }

    public static boolean testConnectionWithServer() throws IOException {
        try {
            return new Socket(Paths.getIP(), 80).isConnected() ? true : false;
        } catch (java.net.ConnectException e) {
            return false;
        }
    }

    public static void cargaUnidades(JTable t) {
        DefaultTableModel modelo = (DefaultTableModel) t.getModel();
        String unidadSeleccionada = null;
        if (t.getSelectedRow() != -1) {
            unidadSeleccionada = String.valueOf(t.getValueAt(t.getSelectedRow(), 0));
        }
        modelo.setRowCount(0);
        File[] unidades = File.listRoots();
        for (File f : unidades) {
            modelo.addRow(new Object[]{f.getPath(), tamaño(f.getFreeSpace()), tamaño(f.getTotalSpace())});
        }
        if (unidadSeleccionada != null) {
            for (int i = 0; i < t.getRowCount(); i++) {
                if (t.getValueAt(i, 0).equals(unidadSeleccionada)) {
                    t.changeSelection(i, 1, false, false);
                }
            }
        }
    }

    public static String tamaño(Long longitud) {
        DecimalFormat df = new DecimalFormat("#.00");
        String str;
        if (longitud > 1024000000) {
            str = df.format(longitud / 1024000000) + " GB";
        } else if (longitud > 1024000) {
            str = df.format(longitud / 1024000) + " MB";
        } else if (longitud > 1024) {
            str = df.format(longitud / 1024) + " KB";
        } else {
            str = df.format(longitud) + " bytes";
        }
        return str;
    }

    public static String fecha() {
        Calendar fecha = new GregorianCalendar();
        return fecha.get(Calendar.DAY_OF_MONTH) + "-" + (fecha.get(Calendar.MONTH) + 1) + "-" + fecha.get(Calendar.YEAR);
    }

    public static String hora() {
        Calendar fecha = new GregorianCalendar();
        return fecha.get(Calendar.HOUR_OF_DAY) + ":" + fecha.get(Calendar.MINUTE) + ":" + fecha.get(Calendar.SECOND);
    }

    public static byte[] obtenerChecksum(String nombreArchivo) throws Exception {
        InputStream fis = new FileInputStream(nombreArchivo);
        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int numRead;
        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);
        fis.close();
        return complete.digest();
    }

    public static String obtenerMD5ComoString(String nombreArchivo) throws Exception {
        byte[] b = obtenerChecksum(nombreArchivo);
        StringBuilder resultado = new StringBuilder();
        for (byte unByte : b) {
            resultado.append(Integer.toString((unByte & 0xff) + 0x100, 16).substring(1));
        }
        return resultado.toString();
    }

    private byte[] getBytes(String path) throws FileNotFoundException {
        try {
            File fichero = new java.io.File(path);
            FileInputStream ficheroStream = new FileInputStream(fichero);
            byte[] contenido = new byte[(int) fichero.length()];
            ficheroStream.read(contenido);
            return contenido;
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        return null;
    }

    public static String[] rutasFich(JTable tbl) {
        String[] aux = new String[tbl.getRowCount()];
        for (int i = 0; i < aux.length; i++) {
            aux[i] = String.valueOf(tbl.getValueAt(i, 0));
        }
        return aux;
    }

    public static void setDefaultFolder(JTextField txt) throws FileNotFoundException, IOException {
        Properties p = new Properties();
        p.load(new FileReader(Paths.getSTARTF()));
        txt.setText(p.getProperty("defaultFolder"));
    }

    public static String getPublicIP() {
        String ip = null;
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            ip = in.readLine();
        } catch (MalformedURLException ex) {
            new RegistroEnvioError(16, ex.getMessage()).start();
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        return ip;
    }

    public static String getNetbiosName() throws UnknownHostException {
        return InetAddress.getLocalHost().getCanonicalHostName();
    }

    public static void recuperarFicheroBD(Connection c, PreparedStatement s, String rutaGuardado, String nombreFichero, int op) {
        InputStream input = null;
        FileOutputStream output = null;
        ResultSet rs = null;
        File file = null;
        try {
            if (op == 0) {
                s = c.prepareStatement(Consultas.getSELECCIONAR_F_ACTU());
                s.setString(1, nombreFichero);
                rs = s.executeQuery();
            } else if (op == 1) {
                s = c.prepareStatement(Consultas.getRESTAURAR_COPIA());
                s.setString(1, nombreFichero);
                s.setInt(2, Integer.parseInt(Usuario.user()[0]));
                rs = s.executeQuery();
            }
            if (rs.first()) {
                file = new File(op == 0 ? nombreFichero : Paths.getTEMPOF() + "\\" + nombreFichero);
                output = new FileOutputStream(file);
                input = rs.getBinaryStream("archivo");
                byte[] buffer = new byte[1024];
                while (input.read(buffer) > 0) {
                    output.write(buffer);
                }
            }
            if (op == 0) {
                Desktop.getDesktop().open(file);
            }
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        } catch (SQLException ex) {
            new RegistroEnvioError(1, ex.getMessage()).start();
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
                if (output != null) {
                    output.close();
                }
                if (s != null) {
                    s.close();
                }
            } catch (IOException ex) {
                new RegistroEnvioError(2, ex.getMessage()).start();
            } catch (SQLException ex) {
                new RegistroEnvioError(1, ex.getMessage()).start();
            }
        }
    }

    public static boolean isNumeric(String str) {
        try {
            return Integer.parseInt(str) >= 0;
        } catch (NumberFormatException ex) {
            new RegistroEnvioError(17, ex.getMessage()).start();
            return false;
        }
    }

    public static boolean comprobarRutas(String str1, String str2) {
        for (int i = 0; i < Main.tareas.size(); i++) {
            if (str1 != null) {
                if (Main.tareas.get(i).getRuta1().equals(str1)) {
                    return false;
                }
            }
            if (str2 != null) {

            }
        }
        return true;
    }

    public static void borrarTempo(File b) {
        if (b.isDirectory()) {
            try {
                for (File listFile : b.listFiles()) {
                    if (listFile.isFile()) {
                        listFile.delete();
                        listFile.deleteOnExit();
                    } else {
                        if (listFile.isDirectory()) {
                            borrarTempo(listFile);
                            listFile.delete();
                            listFile.deleteOnExit();
                        }
                    }
                }
            } catch (NullPointerException ex) {
                new RegistroEnvioError(10, ex.getMessage()).start();
            }
        }
        b.delete();
        b.deleteOnExit();
    }

    public static void verClaveAjustes(JButton jb, JPasswordField txt) {
        if (jb.isSelected()) {
            jb.setSelected(false);
            txt.setEchoChar('*');
        } else {
            jb.setSelected(true);
            txt.setEchoChar((char) 0);
        }
    }
}
