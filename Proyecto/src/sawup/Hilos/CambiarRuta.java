package sawup.Hilos;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import static java.awt.image.ImageObserver.HEIGHT;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import sawup.Clases.Paths;
import sawup.Clases.RegistroL;
import sawup.Clases.Tarea;
import sawup.Main;
import static sawup.Main.btnFormatear;
import static sawup.Main.ok;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class CambiarRuta extends Thread {

    String clonner = "cloner" + Main.tAux + ".bat";
    String eraser = "eraser" + Main.tAux + ".bat";
    String destino;
    String origen;
    int op;

    public CambiarRuta(String origen, String destino, int op) {
        this.origen = origen;
        this.destino = destino;
        this.op = op;
    }

    @Override
    public void run() {
        Tarea t = new Tarea(String.valueOf(Main.tareas.size()), null, getDestino(), null, getOp());
        Main.pblAUX.setIndeterminate(true);
        Main.pblAUX.setStringPainted(true);
        Main.pblAUX.setString(btnFormatear.getText().equals("Formatear") ? "Comenzando los preparativos para el cambio de ruta..." : "Starting preparations for the route change...");
        ArrayList<RegistroL> listaAux = null;
        boolean error = false;
        FileWriter fw = null;
        try {
            fw = new FileWriter(getClonner());
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        Process p = null;

        try (BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write("ROBOCOPY " + getOrigen() + " " + getDestino() + " /E /R:1 /V");
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        try {
            Thread.sleep(1000);
            Main.pblAUX.setString(btnFormatear.getText().equals("Formatear") ? "Moviendo los ficheros a la nueva ubicación..." : "Moving the files to the new location...");
            p = Runtime.getRuntime().exec(getClonner());
            p.waitFor();
        } catch (InterruptedException ex) {
            new RegistroEnvioError(7, ex.getMessage()).start();
        } catch (Error ex) {
            new RegistroEnvioError(11, ex.getMessage()).start();
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            new RegistroEnvioError(7, ex.getMessage()).start();
        }
        Main.pblAUX.setString(btnFormatear.getText().equals("Formatear") ? "Modificando la base de datos..." : "Modifying the database...");
        listaAux = new ArrayList();
        RegistroL a1 = new RegistroL(0, null, null, null, null, null, null, null, null, null, null);
        ObjectContainer bd = Db4oEmbedded.openFile(Paths.getCPHI());
        ObjectSet result = bd.queryByExample(a1);
        if (!result.isEmpty()) {
            while (result.hasNext()) {
                a1 = (RegistroL) result.next();
                RegistroL obAux = a1;
                if (getOrigen().equals(obAux.getRutaDestino())) {
                    obAux.setRutaDestino(getDestino() + "\\");
                }
                listaAux.add(obAux);
            }
        }
        bd.close();
        if (new File(Paths.getCPHI()).exists()) {
            new File(Paths.getCPHI()).delete();
        }
        ObjectContainer bd2 = Db4oEmbedded.openFile(Paths.getCPHI());
        bd2.store(listaAux);
        bd2.close();
        try {
            Metodos.setAjuste(Paths.getSTARTF(), "defaultFolder", getDestino(), "Start File");
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        try {
            fw = new FileWriter(getEraser());
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        try (BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write("RD " + getOrigen() + " /S /Q");
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        Main.pblAUX.setString(btnFormatear.getText().equals("Formatear") ? "Eliminando ruta anterior..." : "Deleting previous route...");
        try {
            p = Runtime.getRuntime().exec(getEraser());
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
        try {
            p.waitFor();
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            new RegistroEnvioError(7, ex.getMessage()).start();
        }
        Main.listaRegistro.clear();
        Main.listaRegistro = listaAux;
        Main.pblAUX.setIndeterminate(false);
        Main.pblAUX.setValue(100);
        Main.pblAUX.setString(btnFormatear.getText().equals("Formatear") ? "Todos los cambios se han aplicado correctamente." : "Changes are being made...");
        JOptionPane.showMessageDialog(null, btnFormatear.getText().equals("Formatear") ? "El cambio de carpeta se ha realizado correctamente." : "The folder change was successful.", "Saw Up", HEIGHT, ok);
        if (new File(getClonner()).exists()) {
            new File(getClonner()).delete();
        }
        if (new File(getEraser()).exists()) {
            new File(getEraser()).delete();
        }
        DefaultTableModel m = (DefaultTableModel) Main.tblRegistroLocal.getModel();
        m.setRowCount(0);
    }

    public String getDestino() {
        return destino;
    }

    public String getOrigen() {
        return origen;
    }

    public int getOp() {
        return op;
    }

    public String getClonner() {
        return clonner;
    }

    public String getEraser() {
        return eraser;
    }

}
