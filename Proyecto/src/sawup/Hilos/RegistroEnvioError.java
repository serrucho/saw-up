package sawup.Hilos;

import java.io.IOException;
import java.sql.SQLException;
import sawup.Clases.Paths;
import sawup.Main;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class RegistroEnvioError extends Thread {
    
    int op;
    String msg;
    
    public RegistroEnvioError(int op, String msg) {
        this.op = op;
        this.msg = msg;
    }
    
    @Override
    public void run() {
        try {
            Main.log.warn(getMsg());
            if (Metodos.getAjuste(Paths.getSTARTF(), "envioDatos").equals("on") && Metodos.testConnectionWithServer() == true) {
                try {
                    String cod = null;
                    switch (getOp()) {
                        case 0:
                            cod = "Parseo";
                            break;
                        case 1:
                            cod = "SQL";
                            break;
                        case 2:
                            cod = "IOException";
                            break;
                        case 3:
                            cod = "ZipException";
                            break;
                        case 4:
                            cod = "SSH";
                            break;
                        case 5:
                            cod = "FileNotFound";
                            break;
                        case 6:
                            cod = "ArrayIndexOut";
                            break;
                        case 7:
                            cod = "Interrupted";
                            break;
                        case 8:
                            cod = "DB4J";
                            break;
                        case 9:
                            cod = "DBClosed";
                            break;
                        case 10:
                            cod = "NullPointer";
                            break;
                        case 11:
                            cod = "Error";
                            break;
                        case 12:
                            cod = "Exception";
                            break;
                        case 13:
                            cod = "Email";
                            break;
                        case 14:
                            cod = "URISyntax";
                            break;
                        case 15:
                            cod = "UnknownHost";
                            break;
                        case 16:
                            cod = "MalformedURL";
                            break;
                        case 17:
                            cod = "NumberFormat";
                            break;                        
                        case 18:
                            cod = "PacketTooBigException";
                            break;
                        default:
                            cod = "Otro";
                            break;
                    }
                    Metodos.connect(Main.conn, cod, getMsg(), 9, null);
                } catch (InterruptedException ex) {
                    new RegistroEnvioError(7, ex.getMessage()).start();
                } catch (IOException ex) {
                    new RegistroEnvioError(2, ex.getMessage()).start();
                } catch (SQLException ex) {
                    new RegistroEnvioError(1, ex.getMessage()).start();
                }
            }
        } catch (IOException ex) {
           new RegistroEnvioError(2, ex.getMessage()).start();
        }
    }
    
    public int getOp() {
        return op;
    }
    
    public String getMsg() {
        return msg;
    }
}
