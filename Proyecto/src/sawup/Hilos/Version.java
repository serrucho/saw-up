package sawup.Hilos;

import java.io.IOException;
import java.sql.SQLException;
import sawup.Main;
import static sawup.Main.cardLayoutConf;
import static sawup.Main.conf;
import static sawup.Main.conn;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Version extends Thread {

    int op;

    public Version(int op) {
        this.op = op;
    }

    @Override
    public void run() {
        try {
            if (getOp() == 0) {
                if (Metodos.testConnectionWithServer()) {
                    Metodos.connect(conn, null, null, 5, null);
                }
            } else if (getOp() == 1) {
                cardLayoutConf.show(conf, "cardConfProgressBar");
                Main.btnComprobarActualizaciones.setEnabled(false);
                Main.pblAUX.setStringPainted(true);
                Main.pblAUX.setIndeterminate(true);
                Main.pblAUX.setString(Main.btnComprobarActualizaciones.getText().equals("Comprobar actualizaciones") ? "Se está conectando al servidor..." : "You are connecting to the server...");
                if (Metodos.testConnectionWithServer()) {
                    Metodos.connect(conn, null, null, 5, null);
                }
            }
        } catch (SQLException ex) {
            new RegistroEnvioError(1, ex.getMessage()).start();
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        } catch (InterruptedException ex) {
            new RegistroEnvioError(7, ex.getMessage()).start();
        } finally {
            cardLayoutConf.show(conf, "cardConfVacio");
            Main.btnComprobarActualizaciones.setEnabled(true);
        }

    }

    public int getOp() {
        return op;
    }

}
