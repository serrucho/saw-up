package sawup.Hilos;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import sawup.Clases.Paths;
import sawup.Clases.Usuario;
import sawup.Main;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class BorradoRegistro extends Thread {

    int op;
    String str;
    JTable tbl;

    public BorradoRegistro(int op, String str, JTable tbl) {
        this.op = op;
        this.str = str;
        this.tbl = tbl;
    }

    @Override
    public void run() {
        DefaultTableModel m = (DefaultTableModel) getTbl().getModel();
        if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null, Main.btnEliminarCopiaLocal.getText().equals("Eliminar") ? "Si eliminas la copia de seguridad " + String.valueOf(getTbl().getValueAt(getTbl().getSelectedRow(), 0)) + " eliminaras todos sus archivos.\n¿Esta seguro que desea eliminar la copia?" : "If you delete the " + String.valueOf(getTbl().getValueAt(Main.tblRegistroLocal.getSelectedRow(), 0)) + " backup you will delete all its files.\nAre you sure you want to delete the copy?", "Saw Up", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, Main.alertaErroneaIco)) {
            if (getOp() == 1) {
                m.removeRow(getTbl().getSelectedRow());
                Main.btnEliminarCopiaLocal.setEnabled(false);
                for (int i = 0; i < Main.listaRegistro.size(); i++) {
                    if (Main.listaRegistro.get(i).getProp().equals(Usuario.user()[1]) && Main.listaRegistro.get(i).getNombreCopia().equals(getStr())) {
                        if (new File(Paths.getCPHI()).exists()) {
                            new File(Paths.getCPHI()).delete();
                        }
                        ObjectContainer bd = Db4oEmbedded.openFile(Paths.getCPHI());
                        Main.listaRegistro.remove(i);
                        bd.store(Main.listaRegistro);
                        bd.close();
                        break;
                    }
                }
                Main.btnEliminarCopiaLocal.setEnabled(true);
            } else if (getOp() == 2) {
                try {
                    Metodos.connect(Main.conn, String.valueOf(getTbl().getValueAt(getTbl().getSelectedRow(), 0)), String.valueOf(getTbl().getSelectedRow()), 6, null);
                } catch (SQLException ex) {
                    new RegistroEnvioError(1, ex.getMessage()).start();
                } catch (InterruptedException ex) {
                      new RegistroEnvioError(7, ex.getMessage()).start();
                } catch (IOException ex) {
                    new RegistroEnvioError(2, ex.getMessage()).start();
                }
            }
        }
    }

    public int getOp() {
        return op;
    }

    public String getStr() {
        return str;
    }

    public JTable getTbl() {
        return tbl;
    }

}
