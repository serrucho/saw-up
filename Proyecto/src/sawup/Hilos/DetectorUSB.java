package sawup.Hilos;

import java.io.File;
import sawup.Main;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class DetectorUSB extends Thread {

    static File[] oldListRoot = File.listRoots();

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(750);
            } catch (InterruptedException ex) {
                 new RegistroEnvioError(7, ex.getMessage()).start();
            }
            if (File.listRoots().length > oldListRoot.length) {
                oldListRoot = File.listRoots();
                c();
            } else if (File.listRoots().length < oldListRoot.length) {
                oldListRoot = File.listRoots();
                c();
            }
        }
    }
    
    public void c(){
        Metodos.cargaUnidades(Main.tblUnidades);
        Metodos.cargaUnidades(Main.tblOrigen);
        Metodos.cargaUnidades(Main.tblDestino);
    }
}
