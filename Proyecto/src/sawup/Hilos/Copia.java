package sawup.Hilos;

import ds.desktop.notify.DesktopNotify;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import net.lingala.zip4j.core.*;
import net.lingala.zip4j.exception.*;
import net.lingala.zip4j.model.*;
import net.lingala.zip4j.util.*;
import sawup.Clases.Paths;
import sawup.Clases.RegistroL;
import sawup.Clases.Tarea;
import sawup.Clases.Usuario;
import sawup.Main;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Copia extends Thread {

    Tarea t;
    String nombreFichero;
    String password;
    String ubicacion;
    String destino;
    String[] rutas;
    String comentario;
    int op;

    public Copia(String nombreFichero, String ubicacion, String destino, String[] rutas, int op, String password, String comentario) {
        this.nombreFichero = nombreFichero;
        this.ubicacion = ubicacion;
        this.destino = destino;
        this.rutas = rutas;
        this.op = op;
        this.password = password;
        this.comentario = comentario;
    }

    public Copia(String nombreFichero, String ubicacion, String destino, int op) {
        this.nombreFichero = nombreFichero;
        this.ubicacion = ubicacion;
        this.destino = destino;
        this.op = op;
    }

    @Override
    public void run() {
        t = new Tarea(String.valueOf(Main.tareas.size()), null, getUbicacion(), getDestino(), getOp());
        try {
            ZipFile zipFile;

            JOptionPane.showMessageDialog(null, Main.btnFormatear.getText().equals("Formatear") ? "Recibirá una notificación en la esquina inferior derecha cuando su tarea finalice." : "You will receive a notification in the lower right corner when your task is complete.", "Saw Up", JOptionPane.YES_OPTION, Main.alertaErroneaIco);
            switch (getOp()) {
                case 1:
                    t.setDesc(Main.btnFormatear.getText().equals("Formatear") ? "Copiando " + new File(getUbicacion()).getName() : "Copying " + new File(getUbicacion()).getName());
                    Tarea.ingresarTarea(t);
                    zipFile = new ZipFile(getUbicacion()
                    );
                    if (comprimir(zipFile, 0) == true) {
                        actualizarHASH(getUbicacion(), getNombreFichero());
                        DesktopNotify.showDesktopMessage(Main.btnFormatear.getText().equals("Formatear") ? "Se ha creado su copia de seguridad " + getNombreFichero() : "Your backup copy " + getNombreFichero() + " has been created", "Saw Up", DesktopNotify.SUCCESS, 10000, new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                try {
                                    Desktop.getDesktop().open(new File(zipFile.getFile().getParent()));
                                } catch (IOException ex) {
                                    new RegistroEnvioError(2, ex.getMessage()).start();
                                }
                            }
                        });
                    } else {
                        DesktopNotify.showDesktopMessage(Main.btnFormatear.getText().equals("Formatear") ? "No se ha podido crear su copia " + getNombreFichero() : "Your copy " + getNombreFichero() + " could not be created", "Saw Up", DesktopNotify.ERROR, 10000, new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                try {
                                    Desktop.getDesktop().open(new File(zipFile.getFile().getParent()));
                                } catch (IOException ex) {
                                    new RegistroEnvioError(2, ex.getMessage()).start();
                                }
                            }
                        });
                    }
                    break;
                case 2:
                    zipFile = new ZipFile(getUbicacion() + "\\" + getNombreFichero());
                    t.setDesc(Main.btnFormatear.getText().equals("Formatear") ? "Restaurando " + new File(getUbicacion()).getName() : "Restoring " + new File(getUbicacion()).getName());
                    Tarea.ingresarTarea(t);
                    descomprimir(zipFile);
                    break;
                case 3:
                    zipFile = new ZipFile(getUbicacion());
                    t.setDesc(Main.btnFormatear.getText().equals("Formatear") ? "Subiendo " + new File(getUbicacion()).getName() : "Storing " + new File(getUbicacion()).getName());
                    Tarea.ingresarTarea(t);
                    if (comprimir(zipFile, 1) == true) {
                        actualizarHASH(getUbicacion(), getNombreFichero());
                        DesktopNotify.showDesktopMessage(Main.btnFormatear.getText().equals("Formatear") ? "Se ha creado su copia de seguridad " + getNombreFichero() : "Your backup copy " + getNombreFichero() + " has been created", "Saw Up", DesktopNotify.SUCCESS, 10000, new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                try {
                                    Desktop.getDesktop().open(new File(zipFile.getFile().getParent()));
                                } catch (IOException ex) {
                                    new RegistroEnvioError(2, ex.getMessage()).start();
                                }
                            }
                        });
                    } else {
                        DesktopNotify.showDesktopMessage(Main.btnFormatear.getText().equals("Formatear") ? "No se ha podido crear su copia " + getNombreFichero() : "Your copy " + getNombreFichero() + " could not be created", "Saw Up", DesktopNotify.ERROR, 10000, new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                try {
                                    Desktop.getDesktop().open(new File(zipFile.getFile().getParent()));
                                } catch (IOException ex) {
                                    new RegistroEnvioError(2, ex.getMessage()).start();
                                }
                            }
                        });
                    }
                    Metodos.connect(null, getUbicacion(), null, 3, null);
                    break;
                case 4:
                    zipFile = new ZipFile(getUbicacion());
                    t.setDesc(Main.btnFormatear.getText().equals("Formatear") ? "Copiando " + new File(getUbicacion()).getName() : "Copying " + new File(getUbicacion()).getName());
                    Tarea.ingresarTarea(t);
                    if (comprimir(zipFile, 0) == true) {
                        DesktopNotify.showDesktopMessage(Main.btnFormatear.getText().equals("Formatear") ? "Se ha creado su copia de seguridad " + getNombreFichero() : "Your backup copy " + getNombreFichero() + " has been created", "Saw Up", DesktopNotify.SUCCESS, 10000, new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                try {
                                    Desktop.getDesktop().open(new File(zipFile.getFile().getParent()));
                                } catch (IOException ex) {
                                    new RegistroEnvioError(2, ex.getMessage()).start();
                                }
                            }
                        });
                    } else {
                        DesktopNotify.showDesktopMessage(Main.btnFormatear.getText().equals("Formatear") ? "No se ha podido crear su copia " + getNombreFichero() : "Your copy " + getNombreFichero() + " could not be created", "Saw Up", DesktopNotify.ERROR, 10000, new java.awt.event.ActionListener() {
                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                try {
                                    Desktop.getDesktop().open(new File(zipFile.getFile().getParent()));
                                } catch (IOException ex) {
                                    new RegistroEnvioError(2, ex.getMessage()).start();
                                }
                            }
                        });
                    }
                    break;
                case 5:
                    t.setDesc(Main.btnFormatear.getText().equals("Formatear") ? "Restaurando " + new File(getUbicacion() + "\\" + getNombreFichero()).getName() : "Restoring " + new File(getUbicacion()).getName());
                    Tarea.ingresarTarea(t);
                    Main.cardLayout.show(Main.slider, "cardResumen");
                    Metodos.connect(Main.conn, getNombreFichero(), getUbicacion(), 7, null);
                    zipFile = new ZipFile(Paths.getTEMPOF() + "\\" + getNombreFichero());
                    destino = getUbicacion() + "\\" + getNombreFichero().split(".zip")[0];
                    descomprimir(zipFile);
                    break;
                default:
                    break;
            }
            Tarea.quitarTarea(t.getId());
        } catch (ZipException ex) {
            new RegistroEnvioError(3, ex.getMessage()).start();
            Tarea.quitarTarea(t.getId());
        } catch (SQLException ex) {
            new RegistroEnvioError(1, ex.getMessage()).start();
            Tarea.quitarTarea(t.getId());
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
            Tarea.quitarTarea(t.getId());
        } catch (InterruptedException ex) {
            new RegistroEnvioError(7, ex.getMessage()).start();
            Tarea.quitarTarea(t.getId());
        } catch (Exception ex) {
            new RegistroEnvioError(12, ex.getMessage()).start();
            Tarea.quitarTarea(t.getId());
        }
    }

    public String getNombreFichero() {
        return nombreFichero;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String[] getRutas() {
        return rutas;
    }

    public int getOp() {
        return op;
    }

    public String getPassword() {
        return password;
    }

    public String getDestino() {
        return destino;
    }

    public String getComentario() {
        return comentario;
    }

    private boolean comprimir(ZipFile zipFile, int op2) {
        try {
            ZipParameters parameters = new ZipParameters();
            ArrayList filesToAdd = new ArrayList();
            for (int i = 0; i < getRutas().length - op2; i++) {
                filesToAdd.add(new File(getRutas()[i]));
            }
            if (getPassword() != null) {
                parameters.setEncryptFiles(true);
                parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
                parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
                parameters.setPassword(getPassword());
            }
            parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
            parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);
            zipFile.addFiles(filesToAdd, parameters);
            if (getComentario() != null) {
                zipFile.setComment(getComentario());
            }
            return true;
        } catch (ZipException ex) {
            Logger.getLogger(Copia.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private void descomprimir(ZipFile zipFile) {
        try {
            if (zipFile.isEncrypted()) {
                String pass = (String) JOptionPane.showInputDialog(null, Main.btnFormatear.getText().equals("Formatear") ? "Introduce la contraseña para tu copia " + getNombreFichero() : "Enter the password for your copy " + getNombreFichero(), "Saw Up", JOptionPane.OK_OPTION, Main.passIco, null, null);
                zipFile.setPassword(pass);
            }
            zipFile.extractAll(getDestino());
            DesktopNotify.showDesktopMessage(Main.btnFormatear.getText().equals("Formatear") ? "Su copia de seguridad " + getNombreFichero() + " ha sido restaurada." : "Your backup " + getNombreFichero() + " has been restored.", "Saw Up", DesktopNotify.SUCCESS, 10000, new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    try {
                        Desktop.getDesktop().open(new File(zipFile.getFile().getParent()));
                    } catch (IOException ex) {
                        new RegistroEnvioError(2, ex.getMessage()).start();
                    }
                }
            });
        } catch (ZipException ex) {
            Tarea.quitarTarea(t.getId());
            Logger.getLogger(Copia.class.getName()).log(Level.SEVERE, null, ex);
            new RegistroEnvioError(3, ex.getMessage()).start();
            DesktopNotify.showDesktopMessage(Main.btnFormatear.getText().equals("Formatear") ? "No se ha podido restaurar su copia " + getNombreFichero() : "Your copy " + getNombreFichero() + " could not be restored", "Saw Up", DesktopNotify.ERROR, 10000);
            if (ex.getMessage().contains("java.io.IOException")) {
                JOptionPane.showMessageDialog(null, Main.btnFormatear.getText().equals("Formatear") ? "Se ha producido un error con el dispositivo de destino." : "An error has occurred with the target device.", "Saw Up", JOptionPane.YES_OPTION, Main.alertaErroneaIco);
            }
            if (ex.getMessage().contains("Wrong Password for file")) {
                JOptionPane.showMessageDialog(null, Main.btnFormatear.getText().equals("Formatear") ? "La contraseña de " + getNombreFichero() + " que ha introducido es errónea." : "The password for " + getNombreFichero() + " you entered is wrong.", "Saw Up", JOptionPane.YES_OPTION, Main.alertaErroneaIco);
            }
        }
    }

    private void actualizarHASH(String rutaFichero, String name) throws Exception {
        String user = Usuario.user()[1];
        String hash = null;
        for (RegistroL listaRegistro : Main.listaRegistro) {
            if (listaRegistro.getProp().equals(user) && listaRegistro.getNombreCopia().equals(name)) {
                listaRegistro.setHash(Metodos.obtenerMD5ComoString(rutaFichero));
                break;
            }
        }
    }
}
