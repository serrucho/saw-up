package sawup.Hilos;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import sawup.Clases.Paths;
import sawup.Clases.Usuario;
import sawup.Main;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Mail extends Thread {

    int op;
    String tempPass;
    String destinatario;
    String contenido;

    public Mail(int op, String destinatario) {
        this.op = op;
        this.destinatario = destinatario;
    }

    public Mail(int op, String destinatario, String contenido, String nombPers) {
        this.op = op;
        this.destinatario = destinatario;
        this.contenido = contenido;
        this.tempPass = nombPers;
    }

    public Mail(int op, String tempPass, String destinatario) {
        this.op = op;
        this.tempPass = tempPass;
        this.destinatario = destinatario;
    }

    @Override
    public void run() {
        try {
//            btnRestartPass1.setEnabled(false);
            final String username = "soft.saw.up@gmail.com";
            final String password = "wushjdhwmzkudwgj";
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
            javax.mail.Session session = javax.mail.Session.getInstance(props, new javax.mail.Authenticator() {
                protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                    return new javax.mail.PasswordAuthentication(username, password);
                }
            });
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setSubject(getAsunto(getOp()));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(getOp() == 2 ? "soft.saw.up@gmail.com" : getDestinatario()));
            MimeMultipart mp = new MimeMultipart();
            BodyPart text = new MimeBodyPart();
            text.setContent(getMensaje(getOp()), "text/html; charset=UTF-8");
            mp.addBodyPart(text);
            message.setContent(mp);
            Transport.send(message);
        } catch (MessagingException ex) {
            new RegistroEnvioError(13, ex.getMessage()).start();
        } catch (SQLException ex) {
            new RegistroEnvioError(1, ex.getMessage()).start();
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        } catch (InterruptedException ex) {
            new RegistroEnvioError(7, ex.getMessage()).start();
        } catch (NullPointerException ex) {
            new RegistroEnvioError(10, ex.getMessage()).start();
        }
    }

    private String getMensaje(int op) throws SQLException, IOException, InterruptedException {
        if (op == 0) {//cambio de contraseña            
            return emailContraseña(getTempPass());
        } else if (op == 1) {//  cambio clave
            return nuevaPass(getTempPass());
        } else if (op == 2) {//contactoAPP
            return emailContacto(getTempPass(), getDestinatario(), getContenido());
        }
        return null;
    }

    private String getAsunto(int op) throws IOException {
        String lan = Metodos.getAjuste(Paths.getSTARTF(), "language");
        switch (getOp()) {
            case 0:
                return lan.equals("spanish") ? "Tu nueva contraseña - Saw Up" : "Your new password - Saw Up";
            case 1:
                return lan.equals("spanish") ? "Nuevo correo de " + getDestinatario() : "Your new password - Saw Up";
            case 2:
                return lan.equals("spanish") ? "Nuevo correo de " + getDestinatario() : "New email from " + getDestinatario();
            default:
                break;
        }
        return null;
    }

    public int getOp() {
        return op;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public String getTempPass() {
        return tempPass;
    }

    public String getContenido() {
        return contenido;
    }

    public static void restablecerCamposContacto() {
        String[] str = Usuario.user();
        Main.txtNombre.setText(str[2]);
        Main.txtApellidos.setText(str[3]);
        Main.txtCorreoElectronico.setText(str[1]);
        Main.txtAsunto.setText("");
        Main.txtMensaje.setText("");
    }

    public String emailContraseña(String pass) throws IOException {
        final String lan = Metodos.getAjuste(Paths.getSTARTF(), "language");
        String estimado = lan.equals("spanish") ? "Estimado usuario:" : "Dear user:";
        String prePass = lan.equals("spanish") ? "Esta es su nueva contraseña para su cuenta en la aplicación Saw Up, recuerde no compartir su nueva contraseña con nadie." : "This is your new password for your account in the Saw Up application, remember not to share your new password with anyone.";
        String siNoFunciona = lan.equals("spanish") ? "Si la nueva contraseña no funciona, pruebe a restaurar la contraseña una vez mas o respondiendo a este correo para que el equipo técnico de Saw Up le responda en el menor tiempo posible." : "If the new password doesn't work, try restoring the password one more time or by replying to this email so that the Saw Up technical team responds in the shortest time possible.";
        String despedida = lan.equals("spanish") ? "Usted ha recibido este correo porque ha seleccionado restablecer la contraseña de su cuenta en la aplicación Saw Up, si usted no lo ha solicitado este cambio, cambie su contraseña." : "You have received this email because you have selected to reset your account password in the Saw Up application, if you have not requested this change, please change your password.";
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "\n"
                + "  <meta charset=\"utf-8\">\n"
                + "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n"
                + "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "  <style type=\"text/css\">\n"
                + "  /**\n"
                + "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n"
                + "   */\n"
                + "  @media screen {\n"
                + "    @font-face {\n"
                + "      font-family: 'Source Sans Pro';\n"
                + "      font-style: normal;\n"
                + "      font-weight: 400;\n"
                + "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n"
                + "    }\n"
                + "\n"
                + "    @font-face {\n"
                + "      font-family: 'Source Sans Pro';\n"
                + "      font-style: normal;\n"
                + "      font-weight: 700;\n"
                + "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n"
                + "    }\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Avoid browser level font resizing.\n"
                + "   * 1. Windows Mobile\n"
                + "   * 2. iOS / OSX\n"
                + "   */\n"
                + "  body,\n"
                + "  table,\n"
                + "  td,\n"
                + "  a {\n"
                + "    -ms-text-size-adjust: 100%; /* 1 */\n"
                + "    -webkit-text-size-adjust: 100%; /* 2 */\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Remove extra space added to tables and cells in Outlook.\n"
                + "   */\n"
                + "  table,\n"
                + "  td {\n"
                + "    mso-table-rspace: 0pt;\n"
                + "    mso-table-lspace: 0pt;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Better fluid images in Internet Explorer.\n"
                + "   */\n"
                + "  img {\n"
                + "    -ms-interpolation-mode: bicubic;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Remove blue links for iOS devices.\n"
                + "   */\n"
                + "  a[x-apple-data-detectors] {\n"
                + "    font-family: inherit !important;\n"
                + "    font-size: inherit !important;\n"
                + "    font-weight: inherit !important;\n"
                + "    line-height: inherit !important;\n"
                + "    color: inherit !important;\n"
                + "    text-decoration: none !important;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Fix centering issues in Android 4.4.\n"
                + "   */\n"
                + "  div[style*=\"margin: 16px 0;\"] {\n"
                + "    margin: 0 !important;\n"
                + "  }\n"
                + "\n"
                + "  body {\n"
                + "    width: 100% !important;\n"
                + "    height: 100% !important;\n"
                + "    padding: 0 !important;\n"
                + "    margin: 0 !important;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Collapse table borders to avoid space between cells.\n"
                + "   */\n"
                + "  table {\n"
                + "    border-collapse: collapse !important;\n"
                + "  }\n"
                + "\n"
                + "  a {\n"
                + "    color: #1a82e2;\n"
                + "  }\n"
                + "\n"
                + "  img {\n"
                + "    height: auto;\n"
                + "    line-height: 100%;\n"
                + "    text-decoration: none;\n"
                + "    border: 0;\n"
                + "    outline: none;\n"
                + "  }\n"
                + "  </style>\n"
                + "</head>\n"
                + "<body style=\"background-color: #e9ecef;\">\n"
                + "\n"
                + "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n"
                + "              <a href=\"192.168.1.55\" target=\"_blank\" style=\"display: inline-block;\">\n"
                + "                <img src=\"https://i.ibb.co/gwRZhxQ/sawup.png\" alt=\"Saw Up\" border=\"0\" style=\"display: block; width: 100%\">\n"
                + "              </a>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "        </table>\n"
                + "      </td>\n"
                + "    </tr>\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n"
                + "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">" + estimado + "</h1>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "        </table>\n"
                + "      </td>\n"
                + "    </tr>\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n"
                + "              <p style=\"margin: 0;\">" + prePass + "</p>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\">\n"
                + "              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                + "                <tr>\n"
                + "                  <td align=\"center\" bgcolor=\"#ffffff\" style=\"padding: 12px;\">\n"
                + "                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "                      <tr>\n"
                + "                        <td align=\"center\" bgcolor=\"#1a82e2\" style=\"border-radius: 6px;\">\n"
                + "                          <a style=\"display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;\">" + pass + "</a>\n"
                + "                        </td>\n"
                + "                      </tr>\n"
                + "                    </table>\n"
                + "                  </td>\n"
                + "                </tr>\n"
                + "              </table>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n"
                + "              <p style=\"margin: 0;\">" + siNoFunciona + "</p>\n"
                + "                          </td>\n"
                + "          </tr>\n"
                + "		          </table>\n"
                + "       </td>\n"
                + "    </tr>\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 24px;\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;\">\n"
                + "              <p style=\"margin: 0;\">" + despedida + "</p>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "        </table>		\n"
                + "      </td>\n"
                + "    </tr>\n"
                + "  </table>\n"
                + "</body>\n"
                + "</html>";
    }

    public String nuevaPass(String pass) throws IOException {
        final String lan = Metodos.getAjuste(Paths.getSTARTF(), "language");
        String estimado = lan.equals("spanish") ? "Estimado usuario:" : "Dear user:";
        String prePass = lan.equals("spanish") ? "Esta es su nueva contraseña para su cuenta en la aplicación Saw Up, recuerde no compartir su nueva contraseña con nadie." : "This is your new password for your account in the Saw Up application, remember not to share your new password with anyone.";
        String siNoFunciona = lan.equals("spanish") ? "Si la nueva contraseña no funciona, pruebe a restaurar la contraseña una vez mas o respondiendo a este correo para que el equipo técnico de Saw Up le responda en el menor tiempo posible." : "If the new password doesn't work, try restoring the password one more time or by replying to this email so that the Saw Up technical team responds in the shortest time possible.";
        String despedida = lan.equals("spanish") ? "Usted ha recibido este correo porque ha seleccionado restablecer la contraseña de su cuenta en la aplicación Saw Up, si usted no lo ha solicitado este cambio, cambie su contraseña." : "You have received this email because you have selected to reset your account password in the Saw Up application, if you have not requested this change, please change your password.";
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "\n"
                + "  <meta charset=\"utf-8\">\n"
                + "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n"
                + "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "  <style type=\"text/css\">\n"
                + "  /**\n"
                + "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n"
                + "   */\n"
                + "  @media screen {\n"
                + "    @font-face {\n"
                + "      font-family: 'Source Sans Pro';\n"
                + "      font-style: normal;\n"
                + "      font-weight: 400;\n"
                + "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n"
                + "    }\n"
                + "\n"
                + "    @font-face {\n"
                + "      font-family: 'Source Sans Pro';\n"
                + "      font-style: normal;\n"
                + "      font-weight: 700;\n"
                + "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n"
                + "    }\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Avoid browser level font resizing.\n"
                + "   * 1. Windows Mobile\n"
                + "   * 2. iOS / OSX\n"
                + "   */\n"
                + "  body,\n"
                + "  table,\n"
                + "  td,\n"
                + "  a {\n"
                + "    -ms-text-size-adjust: 100%; /* 1 */\n"
                + "    -webkit-text-size-adjust: 100%; /* 2 */\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Remove extra space added to tables and cells in Outlook.\n"
                + "   */\n"
                + "  table,\n"
                + "  td {\n"
                + "    mso-table-rspace: 0pt;\n"
                + "    mso-table-lspace: 0pt;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Better fluid images in Internet Explorer.\n"
                + "   */\n"
                + "  img {\n"
                + "    -ms-interpolation-mode: bicubic;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Remove blue links for iOS devices.\n"
                + "   */\n"
                + "  a[x-apple-data-detectors] {\n"
                + "    font-family: inherit !important;\n"
                + "    font-size: inherit !important;\n"
                + "    font-weight: inherit !important;\n"
                + "    line-height: inherit !important;\n"
                + "    color: inherit !important;\n"
                + "    text-decoration: none !important;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Fix centering issues in Android 4.4.\n"
                + "   */\n"
                + "  div[style*=\"margin: 16px 0;\"] {\n"
                + "    margin: 0 !important;\n"
                + "  }\n"
                + "\n"
                + "  body {\n"
                + "    width: 100% !important;\n"
                + "    height: 100% !important;\n"
                + "    padding: 0 !important;\n"
                + "    margin: 0 !important;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Collapse table borders to avoid space between cells.\n"
                + "   */\n"
                + "  table {\n"
                + "    border-collapse: collapse !important;\n"
                + "  }\n"
                + "\n"
                + "  a {\n"
                + "    color: #1a82e2;\n"
                + "  }\n"
                + "\n"
                + "  img {\n"
                + "    height: auto;\n"
                + "    line-height: 100%;\n"
                + "    text-decoration: none;\n"
                + "    border: 0;\n"
                + "    outline: none;\n"
                + "  }\n"
                + "  </style>\n"
                + "</head>\n"
                + "<body style=\"background-color: #e9ecef;\">\n"
                + "\n"
                + "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n"
                + "              <a href=\"192.168.1.55\" target=\"_blank\" style=\"display: inline-block;\">\n"
                + "                <img src=\"https://i.ibb.co/gwRZhxQ/sawup.png\" alt=\"Saw Up\" border=\"0\" style=\"display: block; width: 100%\">\n"
                + "              </a>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "        </table>\n"
                + "      </td>\n"
                + "    </tr>\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n"
                + "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">" + estimado + "</h1>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "        </table>\n"
                + "      </td>\n"
                + "    </tr>\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n"
                + "              <p style=\"margin: 0;\">" + prePass + "</p>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\">\n"
                + "              <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                + "                <tr>\n"
                + "                  <td align=\"center\" bgcolor=\"#ffffff\" style=\"padding: 12px;\">\n"
                + "                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "                      <tr>\n"
                + "                        <td align=\"center\" bgcolor=\"#1a82e2\" style=\"border-radius: 6px;\">\n"
                + "                          <a style=\"display: inline-block; padding: 16px 36px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; color: #ffffff; text-decoration: none; border-radius: 6px;\">" + pass + "</a>\n"
                + "                        </td>\n"
                + "                      </tr>\n"
                + "                    </table>\n"
                + "                  </td>\n"
                + "                </tr>\n"
                + "              </table>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n"
                + "              <p style=\"margin: 0;\">" + siNoFunciona + "</p>\n"
                + "                          </td>\n"
                + "          </tr>\n"
                + "		          </table>\n"
                + "       </td>\n"
                + "    </tr>\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 24px;\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;\">\n"
                + "              <p style=\"margin: 0;\">" + despedida + "</p>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "        </table>		\n"
                + "      </td>\n"
                + "    </tr>\n"
                + "  </table>\n"
                + "</body>\n"
                + "</html>";
    }

    public String emailContacto(String nombre, String correo, String contenido) throws IOException {
        final String lan = Metodos.getAjuste(Paths.getSTARTF(), "language");
        String etiquetaPrin = lan.equals("spanish") ? "Nuevo correo de " + nombre : "New email from " + nombre;
        String remitente = lan.equals("spanish") ? "Remitente: " + correo : "From: " + correo;
        String despedida = lan.equals("spanish") ? "Para el equipo técnico de Saw Up." : "For the Saw Up technical team.";
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "  <meta charset=\"utf-8\">\n"
                + "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n"
                + "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "  <style type=\"text/css\">\n"
                + "  /**\n"
                + "   * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n"
                + "   */\n"
                + "  @media screen {\n"
                + "    @font-face {\n"
                + "      font-family: 'Source Sans Pro';\n"
                + "      font-style: normal;\n"
                + "      font-weight: 400;\n"
                + "      src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');\n"
                + "    }\n"
                + "\n"
                + "    @font-face {\n"
                + "      font-family: 'Source Sans Pro';\n"
                + "      font-style: normal;\n"
                + "      font-weight: 700;\n"
                + "      src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');\n"
                + "    }\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Avoid browser level font resizing.\n"
                + "   * 1. Windows Mobile\n"
                + "   * 2. iOS / OSX\n"
                + "   */\n"
                + "  body,\n"
                + "  table,\n"
                + "  td,\n"
                + "  a {\n"
                + "    -ms-text-size-adjust: 100%; /* 1 */\n"
                + "    -webkit-text-size-adjust: 100%; /* 2 */\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Remove extra space added to tables and cells in Outlook.\n"
                + "   */\n"
                + "  table,\n"
                + "  td {\n"
                + "    mso-table-rspace: 0pt;\n"
                + "    mso-table-lspace: 0pt;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Better fluid images in Internet Explorer.\n"
                + "   */\n"
                + "  img {\n"
                + "    -ms-interpolation-mode: bicubic;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Remove blue links for iOS devices.\n"
                + "   */\n"
                + "  a[x-apple-data-detectors] {\n"
                + "    font-family: inherit !important;\n"
                + "    font-size: inherit !important;\n"
                + "    font-weight: inherit !important;\n"
                + "    line-height: inherit !important;\n"
                + "    color: inherit !important;\n"
                + "    text-decoration: none !important;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Fix centering issues in Android 4.4.\n"
                + "   */\n"
                + "  div[style*=\"margin: 16px 0;\"] {\n"
                + "    margin: 0 !important;\n"
                + "  }\n"
                + "\n"
                + "  body {\n"
                + "    width: 100% !important;\n"
                + "    height: 100% !important;\n"
                + "    padding: 0 !important;\n"
                + "    margin: 0 !important;\n"
                + "  }\n"
                + "\n"
                + "  /**\n"
                + "   * Collapse table borders to avoid space between cells.\n"
                + "   */\n"
                + "  table {\n"
                + "    border-collapse: collapse !important;\n"
                + "  }\n"
                + "\n"
                + "  a {\n"
                + "    color: #1a82e2;\n"
                + "  }\n"
                + "\n"
                + "  img {\n"
                + "    height: auto;\n"
                + "    line-height: 100%;\n"
                + "    text-decoration: none;\n"
                + "    border: 0;\n"
                + "    outline: none;\n"
                + "  }\n"
                + "  </style>\n"
                + "</head>\n"
                + "<body style=\"background-color: #e9ecef;\">\n"
                + "\n"
                + "  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"center\" valign=\"top\" style=\"padding: 36px 24px;\">\n"
                + "              <a href=\"192.168.1.55\" target=\"_blank\" style=\"display: inline-block;\">\n"
                + "                <img src=\"https://i.ibb.co/gwRZhxQ/sawup.png\" alt=\"Logo\" border=\"0\" style=\"display: block; width: 90%\">\n"
                + "              </a>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "        </table>\n"
                + "      </td>\n"
                + "    </tr>\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;\">\n"
                + "              <h1 style=\"margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;\">" + etiquetaPrin + "</h1>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "        </table>\n"
                + "      </td>\n"
                + "    </tr>\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n"
                + "              <p style=\"margin: 0;\">" + remitente + "</p>\n"
                + "			  <p/>\n"
                + "          <p style=\"margin: 0;\">" + contenido + "</p>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "          	  \n"
                + "		   <tr>\n"
                + "            <td align=\"left\" bgcolor=\"#ffffff\" style=\"padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;\">\n"
                + "              <p/>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "          <tr>\n"
                + "        </table>\n"
                + "       </td>\n"
                + "    </tr>\n"
                + "    <tr>\n"
                + "      <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 24px;\">\n"
                + "        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px;\">\n"
                + "          <tr>\n"
                + "            <td align=\"center\" bgcolor=\"#e9ecef\" style=\"padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;\">\n"
                + "              <p style=\"margin: 0;\">" + despedida
                + "</p>\n"
                + "            </td>\n"
                + "          </tr>\n"
                + "        </table>		\n"
                + "      </td>\n"
                + "    </tr>\n"
                + "  </table>\n"
                + "</body>\n"
                + "</html>";
    }

}
