package sawup.Hilos;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import sawup.Clases.Paths;
import sawup.Clases.RegistroL;
import sawup.Clases.Usuario;
import sawup.Main;
import static sawup.Main.tblRegistroLocal;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Selector extends Thread {

    int op;
    private int val = 0;

    public Selector(int op) {
        this.op = op;
    }

    @Override
    public void run() {
        try {
            DefaultTableModel m;
            String user[] = Usuario.user();
            if (getOp() == 1) {
                Date fechaFinal = Main.dateFormat.parse(Metodos.fecha());
                int dias = Integer.parseInt(Metodos.getAjuste(Paths.getSTARTF(), "days"));
                m = (DefaultTableModel) tblRegistroLocal.getModel();
                m.setRowCount(0);
                Main.contLoc = 0;
                setBotonLocal(false);
                for (RegistroL listaRegistro : Main.listaRegistro) {
                    try {
                        Date fechaInicial = Main.dateFormat.parse(listaRegistro.getFecha());
                        if (listaRegistro.getProp().equals(user[1]) && new File(listaRegistro.getRutaDestino() + listaRegistro.getNombreCopia()).exists()) {
                            int act = (int) ((fechaFinal.getTime() - fechaInicial.getTime()) / 86400000);
                            if (act > dias) {
                                val++;
                            }
                            m.addRow(new Object[]{listaRegistro.getNombreCopia(), listaRegistro.getRutaDestino(), new File(listaRegistro.getRutaDestino() + listaRegistro.getNombreCopia()).exists() ? Metodos.tamaño(new File(listaRegistro.getRutaDestino() + listaRegistro.getNombreCopia()).length()) : 0, listaRegistro.getFecha(), listaRegistro.getHora(), act, false});
                            Main.contLoc++;
                            Thread.sleep(150);
                        }
                    } catch (ParseException ex) {
                        new RegistroEnvioError(0, ex.getMessage()).start();
                    } catch (InterruptedException ex) {
                        new RegistroEnvioError(7, ex.getMessage()).start();
                    }
                }
                setBotonLocal(true);
                if (val != 0) {
                    JOptionPane.showMessageDialog(null, Main.btnFormatear.getText().equals("Formatear") ? "Hace más de " + dias + " días que no haces " + val + " copias de seguridad." : "It has been more than " + dias + " days that you have not made " + val + " backup copies.", "Saw Up", JOptionPane.YES_OPTION, Main.alertaErroneaIco);
                }
            } else if (getOp() == 2) {
                setBotonLinea(false);
                try {
                    Metodos.connect(Main.conn, user[0], null, 4, null);
                } catch (InterruptedException ex) {
                    new RegistroEnvioError(7, ex.getMessage()).start();
                } catch (SQLException ex) {
                    new RegistroEnvioError(1, ex.getMessage()).start();
                } catch (IOException ex) {
                    new RegistroEnvioError(2, ex.getMessage()).start();
                } finally {
                    setBotonLinea(true);
                }
            }
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        } catch (ParseException ex) {
            new RegistroEnvioError(0, ex.getMessage()).start();
        }
    }

    public int getOp() {
        return op;
    }

    private void setBotonLocal(Boolean b) {
        Main.btnAñadirCopia.setEnabled(b);
        Main.btnRestaurarCopiaLocal.setEnabled(b);
        Main.btnHacerSeleccionadasLocal.setEnabled(b);
        Main.btnEliminarCopiaLocal.setEnabled(b);
        Main.btnEditarCopia.setEnabled(b);
    }

    private void setBotonLinea(Boolean b) {
        Main.btnAgregarCopiaLinea.setEnabled(b);
        Main.btnRestaurarLinea.setEnabled(b);
        Main.btnEliminarCopiaLinea.setEnabled(b);
    }
}
