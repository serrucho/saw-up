package sawup.Hilos;

import java.io.IOException;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import sawup.Main;
import static sawup.Main.alertaErroneaIco;
import static sawup.Main.btnRegistrar;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Cambio extends Thread {

    String str1;
    String str2;
    int op;

    public Cambio(String str1, String str2, int op) {
        this.str1 = str1;
        this.str2 = str2;
        this.op = op;
    }

    public Cambio(String str1, int op) {
        this.str1 = str1;
        this.op = op;
    }

    @Override
    public void run() {
        Main.btnCambInfo.setEnabled(false);
        Main.btnRestCampoConfDat.setEnabled(false);

        try {
            if (Metodos.testConnectionWithServer() == true) {
                if (getOp() == 0) {
                    Metodos.connect(Main.conn, getStr1(), getStr2(), 10, null);
                } else if (getOp() == 1) {
                    Metodos.connect(Main.conn, getStr1(), null, 2, null);
                }
            } else {
                JOptionPane.showMessageDialog(null, btnRegistrar.getText().equals("Registrarse") ? "No se puede conectar con el servidor." : "Can not connect to server.", "Saw Up", JOptionPane.YES_OPTION, alertaErroneaIco);
            }
        } catch (SQLException ex) {
            new RegistroEnvioError(1, ex.getMessage()).start();
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        } catch (InterruptedException ex) {
            new RegistroEnvioError(8, ex.getMessage()).start();
        } finally {
            Main.btnCambInfo.setEnabled(true);
            Main.btnRestCampoConfDat.setEnabled(true);
            Main.jTextField1.setEditable(false);
            Main.jTextField2.setEditable(false);
            Main.jPasswordField1.setEditable(false);
            Main.jPasswordField2.setEditable(false);
            Main.btnConfHabNom.setSelected(false);
            Main.btnConfHabilPass.setSelected(false);
            Main.jPasswordField1.setText("");
            Main.jPasswordField2.setText("");
        }
    }

    public String getStr1() {
        return str1;
    }

    public String getStr2() {
        return str2;
    }

    public int getOp() {
        return op;
    }

}
