package sawup.Hilos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JOptionPane;
import sawup.Clases.Tarea;
import sawup.Main;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class CorregirUnidad extends Thread {

    String unidad;
    String corrector = "corrector" + Main.tareas.size() + ".bat";

    public CorregirUnidad(String unidad) {
        this.unidad = unidad;
    }

    @Override
    public void run() {
        Tarea t = new Tarea(String.valueOf(Main.tareas.size()), Main.btnCorregir.getText().equals("Corregir errores") ? "Corrigiendo errores en " + getUnidad() : "Correcting errors in " + getUnidad(), getUnidad(), null, 8);
        String msg = "";
        try {
            Tarea.ingresarTarea(t);
            FileWriter fw = null;
            Process p = null;
            fw = new FileWriter(getCorrector());
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                bw.write("CHKDSK /X /F " + getUnidad());
                bw.close();
            }
            p = Runtime.getRuntime().exec(getCorrector());
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream(), "UTF-8"));
            while (br.readLine() != null) {
                msg = br.readLine();
            }
            switch (p.exitValue()) {
                case 0:
                    JOptionPane.showMessageDialog(null, Main.btnFormatear.getText().equals("Formatear") ? "No se han encontrado errores en la unidad " + getUnidad() + "." : "No errors were found on drive " + getUnidad() + ".", "Saw Up", JOptionPane.OK_OPTION, Main.ok);
                    break;
                case 1:
                    JOptionPane.showMessageDialog(null, Main.btnFormatear.getText().equals("Formatear") ? "Se han encontrado errores en el disco " + getUnidad() + " y se han reparado correctamente." : "Errors have been found on disk " + getUnidad() + " and have been successfully repaired.", "Saw Up", JOptionPane.OK_OPTION, Main.ok);
                    break;
                case 2:
                case 3:
                    JOptionPane.showMessageDialog(null, Main.btnFormatear.getText().equals("Formatear") ? "Se han encontrado errores, pero no ha sido posible solucionarlos." : "Errors were found, but could not be fixed.", "Saw Up", JOptionPane.YES_OPTION, Main.alertaErroneaIco);
                    break;
            }
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        } finally {
            Tarea.quitarTarea(t.getId());
            if (new File(getCorrector()).exists()) {
                new File(getCorrector()).delete();
            }
        }
    }

    public String getUnidad() {
        return unidad;
    }

    public String getCorrector() {
        return corrector;
    }

}
