package sawup.Hilos;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.ext.Db4oIOException;
import javax.swing.table.DefaultTableModel;
import sawup.Clases.Paths;
import sawup.Clases.RegistroL;
import sawup.Main;
import static sawup.Main.tblRegistroLocal;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class CargaBD extends Thread {

    public CargaBD() {
    }

    @Override
    public void run() {
        Main.btnCopiaLocal.setEnabled(false);
        Main.btnCopiaEnLinea.setEnabled(false);
        ObjectContainer bd = Db4oEmbedded.openFile(Paths.getCPHI());
        try {
            if (!Main.listaRegistro.isEmpty()) {
                Main.listaRegistro.clear();
            }
            Main.modelo = (DefaultTableModel) tblRegistroLocal.getModel();
            Main.modelo.setRowCount(0);
            RegistroL a1 = new RegistroL(0, null, null, null, null, null, null, null, null, null, null);
            ObjectSet result = bd.queryByExample(a1);
            if (!result.isEmpty()) {
                while (result.hasNext()) {
                    a1 = (RegistroL) result.next();
                    Main.listaRegistro.add(a1);
                }
            }
        } catch (DatabaseClosedException ex) {
            new RegistroEnvioError(9, ex.getMessage()).start();
        } catch (Db4oIOException ex) {
            new RegistroEnvioError(8, ex.getMessage()).start();
        } catch (NullPointerException ex) {
            new RegistroEnvioError(10, ex.getMessage()).start();
        } catch(ArrayIndexOutOfBoundsException ex){
             new RegistroEnvioError(6, ex.getMessage()).start();
        }finally {
            bd.close();
            Main.btnCopiaLocal.setEnabled(true);
            Main.btnCopiaEnLinea.setEnabled(true);
        }

    }

}
