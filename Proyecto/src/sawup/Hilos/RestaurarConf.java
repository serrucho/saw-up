package sawup.Hilos;

import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;
import sawup.Clases.Paths;
import sawup.Main;
import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class RestaurarConf extends Thread {

    @Override
    public void run() {
        try {
            if (!new File(System.getProperty("user.home") + File.separatorChar + "Saw-Up-old").exists()) {
                new File(Metodos.getAjuste(Paths.getSTARTF(), "defaultFolder")).renameTo(new File(System.getProperty("user.home") + File.separatorChar + "Saw-Up-old"));
                if (new File(Paths.getMAIN()).exists()) {
                    Metodos.borrarTempo(new File(Paths.getMAIN()));
                    Paths.sawupDirectory();
                    JOptionPane.showMessageDialog(null, Main.btnRegistrar.getText().equals("Registrarse") ? "Se han restablecido los valores predeterminados." : "The default values ​​have been restored.", "Saw Up", JOptionPane.YES_OPTION, Main.ok);
                    Main.cardLayout.show(Main.slider, "cardLogin");
                    new File(Metodos.getAjuste(Paths.getSTARTF(), "defaultFolder")).createNewFile();
                }
            } else {
                JOptionPane.showMessageDialog(null, Main.btnRegistrar.getText().equals("Registrarse") ? "Ya existe la carpeta Saw-Up-old, asegúrese de que no existe y vuelva a intentarlo." : "The Saw-Up-old folder already exists, make sure it does not exist and try again.", "Saw Up", JOptionPane.YES_OPTION, Main.alertaErroneaIco);
            }
        } catch (IOException ex) {
            new RegistroEnvioError(2, ex.getMessage()).start();
        }
    }
}
