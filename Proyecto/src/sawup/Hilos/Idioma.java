package sawup.Hilos;

import java.io.IOException;
import javax.swing.JProgressBar;
import sawup.Clases.Paths;
import sawup.Main;
import static sawup.Main.*;
import sawup.Metodos.Metodos;
import sawup.Metodos.Text;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Idioma extends Thread {

    String idioma;
    int op;
    JProgressBar jpb;

    public Idioma(String idioma, int op, JProgressBar jpb) {
        this.idioma = idioma;
        this.op = op;
        this.jpb = jpb;
    }

    @Override
    public void run() {

        getJpb().setVisible(true);
        getJpb().setIndeterminate(true);
        getJpb().setStringPainted(true);
        getJpb().setString(btnCambiarIdioma.getText().equals("Cambiar idioma") ? "Esta operación podría llevar un tiempo..." : "This operation could take a while...");

        switch (getOp()) {
            case 0:
                try {
                    btnAplicarIdioma.setEnabled(false);
                    Metodos.setAjuste(Paths.getSTARTF(), "language", getIdioma(), "Start File");
                    Text.text(Main.holder);
                    btnAplicarIdioma.setEnabled(true);
                    lblQueIdioma.setText(btnCambiarIdioma.getText().equals("Cambiar idioma") ? "El idioma que se esta usando es: " + (getIdioma().equals("spanish") ? "Español" : "Inglés") : "The language that is being used is: " + (getIdioma().equals("spanish") ? "Spanish" : "English"));

                } catch (IOException ex) {
                   new RegistroEnvioError(2, ex.getMessage()).start();
                }
                break;
            case 1:
                cardLayoutConf.show(Main.conf, "cardConfProgressBar");
                Text.traduccion("spanish");
                Text.traduccion("english");
                Text.text(Main.holder);
                break;
        }
        try {
            Thread.sleep(1500);
            getJpb().setIndeterminate(false);
            getJpb().setValue(100);
            switch (getOp()) {
                case 0:
                    getJpb().setString(btnCambiarIdioma.getText().equals("Cambiar idioma") ? "Se han aplicado los cambios seleccionados." : "The selected changes have been applied.");
                    break;
                case 1:
                    getJpb().setString(btnCambiarIdioma.getText().equals("Cambiar idioma") ? "Se han restaurado las traducciones de la aplicación." : "The translations of the application have been restored.");
                    break;
            }
        } catch (InterruptedException ex) {
             new RegistroEnvioError(7, ex.getMessage()).start();
        }

    }

    public String getIdioma() {
        return idioma;
    }

    public int getOp() {
        return op;
    }

    public JProgressBar getJpb() {
        return jpb;
    }

}
