package sawup.Hilos;

import ds.desktop.notify.DesktopNotify;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;
import sawup.Clases.*;
import sawup.Main;
import static sawup.Main.*;
import sawup.Metodos.*;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Clon extends Thread {

    String origen;
    String destino;
    String nombre;
    int op;
    String clonner = "cloner" + Main.tAux + ".bat";
    String formatter = "format" + Main.tAux + ".bat";
    String eraser = "eraser" + Main.tAux + ".bat";

    public Clon(String origen, String destino, int op) {
        this.origen = origen;
        this.destino = destino;
        this.op = op;
    }

    public Clon(String origen, String destino, String nombre, int op) {
        this.origen = origen;
        this.destino = destino;
        this.nombre = nombre;
        this.op = op;
    }

    @Override
    public void run() {
        if (Metodos.comprobarRutas(getOrigen(), getDestino())) {
            Tarea t = new Tarea(String.valueOf(Main.tareas.size()), null, getDestino(), null, getOp() == 1 ? 7 : 0);
            ArrayList<RegistroL> listaAux = null;
            boolean error = false;
            FileWriter fw = null;
            Process p = null;
            try {
                Main.pblAUX.setString(btnFormatear.getText().equals("Formatear") ? "Se están realizando los preparativos para los cambios…" : "");
                if (!new File(getDestino()).exists() && getOp() == 0) {
                    new File(getDestino()).mkdir();
                }
                t.setDesc(Main.btnFormatear.getText().equals("Formatear") ? "Clonando " + getOrigen() + " en " + getDestino() : "Cloning " + getOrigen() + " into " + getDestino());
                Tarea.ingresarTarea(t);
                fw = new FileWriter(getFormatter());
                try (BufferedWriter bw = new BufferedWriter(fw)) {
                    bw.write("Format /y " + getDestino().replace("\\", "") + " /q /fs:NTFS /v:" + getNombre());
                    bw.close();
                }
                try {
                    p = Runtime.getRuntime().exec(getFormatter());
                    p.waitFor();
                    if (p.exitValue() != 0) {
                        error = true;
                        JOptionPane.showMessageDialog(null, btnFormatear.getText().equals("Formatear") ? "Su unidad ha sido clonada con algun error, \nse recomienda volver a clonar la unidad." : "Errors occurred while cloning the drive, \nit is recommended that you clone the drive again.", "Saw Up", JOptionPane.OK_OPTION, Main.alertaErroneaIco);
                    }
                } catch (InterruptedException ex) {
                    new RegistroEnvioError(7, ex.getMessage()).start();
                }
                if (error == false) {
                    fw = new FileWriter(getClonner());
                    try (BufferedWriter bw = new BufferedWriter(fw)) {
                        bw.write("ROBOCOPY " + getOrigen() + "\\ " + getDestino() + "\\ ");
                    }
                    try {
                        Thread.sleep(1000);
                        p = Runtime.getRuntime().exec(getClonner());
                        p.waitFor();
                    } catch (InterruptedException ex) {
                        new RegistroEnvioError(7, ex.getMessage()).start();
                    } catch (Error ex) {
                        new RegistroEnvioError(11, ex.getMessage()).start();
                    }
                    if (p.exitValue() == 1) {
                        DesktopNotify.showDesktopMessage("Saw Up", btnFormatear.getText().equals("Formatear") ? "Se ha clonado correctamente su dispositivo." : "Your device has been successfully cloned.", DesktopNotify.SUCCESS);
                    } else {
                        switch (getOp()) {
                            case 0:
                                JOptionPane.showMessageDialog(null, btnFormatear.getText().equals("Formatear") ? "Ha ocurrido algún error mientras se cambiaba la carpeta.\nLos cambios no han sido aplicados." : "An error occurred while changing the folder.\nThe changes have not been applied.", "Saw Up", JOptionPane.OK_OPTION, Main.alertaErroneaIco);
                                fw = new FileWriter(getEraser());
                                try (BufferedWriter bw = new BufferedWriter(fw)) {
                                    bw.write("RD " + getDestino() + "\\ /S /Q");
                                }
                                Main.pblAUX.setString(btnFormatear.getText().equals("Formatear") ? "Eliminando ruta anterior..." : "Deleting previous route...");
                                p = Runtime.getRuntime().exec(getClonner());
                                p.waitFor();
                                break;
                            case 1:
                                JOptionPane.showMessageDialog(null, btnFormatear.getText().equals("Formatear") ? "Su unidad ha sido clonada con algun error, \nse recomienda volver a clonar la unidad." : "Errors occurred while cloning the drive, \nit is recommended that you clone the drive again.", "Saw Up", JOptionPane.OK_OPTION, Main.alertaErroneaIco);
                                break;
                        }
                    }
                }
            } catch (IOException ex) {
                new RegistroEnvioError(2, ex.getMessage()).start();
            } catch (InterruptedException ex) {
                new RegistroEnvioError(7, ex.getMessage()).start();
            } finally {
                try {
                    fw.close();
                } catch (IOException ex) {
                    new RegistroEnvioError(2, ex.getMessage()).start();
                }
                if (new File(getClonner()).exists()) {
                    new File(getClonner()).delete();
                }
                if (new File(getFormatter()).exists()) {
                    new File(getFormatter()).delete();
                }
                Tarea.quitarTarea(t.getId());
            }
        } else {
            JOptionPane.showMessageDialog(null, Main.btnFormatear.getText().equals("") ? "Alguna de las rutas seleccionadas esta siendo utilizada por otra tarea.\nLa tarea no se ha ejecutado." : "Some of the selected routes are being used by another task.\nThe task has not been run.", "Saw Up", JOptionPane.OK_OPTION, Main.alertaErroneaIco);
        }
    }

    public String getOrigen() {
        return origen;
    }

    public String getDestino() {
        return destino;
    }

    public int getOp() {
        return op;
    }

    public String getClonner() {
        return clonner;
    }

    public String getFormatter() {
        return formatter;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEraser() {
        return eraser;
    }

}
