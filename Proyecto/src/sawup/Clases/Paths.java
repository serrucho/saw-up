package sawup.Clases;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import sawup.Hilos.RegistroEnvioError;
import sawup.Metodos.Metodos;
import sawup.Metodos.Text;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Paths {

    static final String MAIN = "./files";
    static final String CONFIGF = MAIN + "/config";
    static final String LANGUAGEF = MAIN + "/language";
    static final String RECORDF = MAIN + "/history";
    static final String TEMPOF = MAIN + "/tempo";
    static final String LOGF = MAIN + "/log";
    static final String VERF = CONFIGF + "/vers.pdp";
    static final String STARTF = CONFIGF + "/config.properties";
    static final String CONP = CONFIGF + "/server.properties";
    static final String USERDAT = CONFIGF + "/user.dat";
    static final String ESPF = LANGUAGEF + "/spanish.properties";
    static final String ENGF = LANGUAGEF + "/english.properties";
    static final String CPHI = RECORDF + "/localcopy.pdp";
    static final String IPASS = "/sawup/Imagenes/candado.png";
    static final String IALERT = "/sawup/Imagenes/alerta.png";
    static final String IOK = "/sawup/Imagenes/ok.png";
    static final String DF = System.getProperty("user.home") + File.separatorChar + "Saw-Up";
    static final String MANUALES = "./Manual_Saw_Up_1.0.0.0_Español.pdf";
    static final String MANUALEN = "./Saw_Up_Manual_1.0.0.0_English.pdf";

    public static String getIP() throws IOException {
        return Metodos.getAjuste(getCONP(), "server");
    }

    public static String getMAIN() {
        return MAIN;
    }

    public static String getCONFIGF() {
        return CONFIGF;
    }

    public static String getLANGUAGEF() {
        return LANGUAGEF;
    }

    public static String getRECORDF() {
        return RECORDF;
    }

    public static String getTEMPOF() {
        return TEMPOF;
    }

    public static String getSTARTF() {
        return STARTF;
    }

    public static String getUSERDAT() {
        return USERDAT;
    }

    public static String getESPF() {
        return ESPF;
    }

    public static String getENGF() {
        return ENGF;
    }

    public static String getCPHI() {
        return CPHI;
    }

    public static String getIPASS() {
        return IPASS;
    }

    public static String getIALERT() {
        return IALERT;
    }

    public static String getVERF() {
        return VERF;
    }

    public static String getIOK() {
        return IOK;
    }

    public static String getDF() {
        return DF;
    }

    public static String getLOGF() {
        return LOGF;
    }

    public static String getCONP() {
        return CONP;
    }

    public static String getMANUALES() {
        return MANUALES;
    }

    public static String getMANUALEN() {
        return MANUALEN;
    }

    public static void sawupDirectory() throws IOException {
        File fich = new File(getMAIN());
        if (!fich.exists()) {
            fich.mkdir();
        }
        subDir(fich);
    }

    public static void subDir(File fil) throws IOException {
        dir(fil);
        files(fil);
    }

    private static void dir(File fil) {
        fil = new File(getCONFIGF());
        if (!fil.exists()) {
            fil.mkdir();
        }
        fil = new File(getRECORDF());
        if (!fil.exists()) {
            fil.mkdir();
        }
        fil = new File(getLANGUAGEF());
        if (!fil.exists()) {
            fil.mkdir();
        }
        fil = new File(getTEMPOF());
        if (!fil.exists()) {
            fil.mkdir();
        }
        fil = new File(getDF());
        if (!fil.exists()) {
            fil.mkdir();
        }
        fil = new File(getLOGF());
        if (!fil.exists()) {
            fil.mkdir();
        }
    }

    private static void files(File fil) throws IOException {
        if (!new File(getUSERDAT()).exists()) {
            new File(getUSERDAT()).createNewFile();
        }
        fil = new File(getSTARTF());
        if (!fil.exists()) {
            try {
                fil.createNewFile();
                Properties p = new Properties();
                p.load(new FileReader(getSTARTF()));
                p.setProperty("login", "off");
                p.setProperty("language", "spanish");
                p.setProperty("theme", "MistAquaSkin");
                p.setProperty("defaultFolder", getDF());
                p.setProperty("act", "off");
                p.setProperty("not", "light");
                p.setProperty("days", "7");
                p.setProperty("ver", "1.0.0.0");
                p.setProperty("envioDatos", "on");
                p.store(new BufferedWriter(new FileWriter(getSTARTF())), "Start File");
            } catch (IOException ex) {
                new RegistroEnvioError(2, ex.getMessage()).start();
            }
        }
        fil = new File(getCONP());
        if (!fil.exists()) {
            try {
                fil.createNewFile();
                Properties p = new Properties();
                p.load(new FileReader(getCONP()));
                p.setProperty("mysql", "sawup");
                p.setProperty("mysql2", "admin");
                p.setProperty("ssh", "Fran");
                p.setProperty("ssh2", "admin");
                p.setProperty("server", "sawup.ddns.net");
                p.store(new BufferedWriter(new FileWriter(getCONP())), "Connection File");
            } catch (IOException ex) {
                new RegistroEnvioError(2, ex.getMessage()).start();
            }
        }

        fil = new File(getESPF());
        if (!fil.exists()) {
            fil.createNewFile();
            Text.traduccion("spanish");
        }
        fil = new File(getENGF());
        if (!fil.exists()) {
            fil.createNewFile();
            Text.traduccion("english");
        }
    }
}
