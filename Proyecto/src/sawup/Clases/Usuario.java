package sawup.Clases;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.Serializable;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Usuario implements Serializable {

    int id;
    String email;
    String nombre;
    String apellidos;
    String password;

    public Usuario() {
    }

    public Usuario(int id, String email, String nombre, String apellidos) {
        this.id = id;
        this.email = email;
        this.nombre = nombre;
        this.apellidos = apellidos;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public static String[] user() {
        String str[] = new String[4];
        ObjectContainer bd = Db4oEmbedded.openFile(Paths.getUSERDAT());
        Usuario a2 = new Usuario(0, null, null, null);
        ObjectSet result2 = bd.queryByExample(a2);
        while (result2.hasNext()) {
            a2 = (Usuario) result2.next();
            str[0] = String.valueOf(a2.getId());
            str[1] = a2.getEmail();
            str[2] = a2.getNombre();
            str[3] = a2.getApellidos();
        }
        bd.close();
        return str;
    }

}
