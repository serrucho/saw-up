package sawup.Clases;

import javax.swing.table.DefaultTableModel;
import sawup.Main;
import static sawup.Main.tblTareas;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Tarea {

    String id;
    String desc;
    String ruta1;
    String ruta2;
    int op;

    public Tarea(String id, String desc, String ruta, String inf, int op) {
        this.ruta1 = ruta;
        this.ruta2 = inf;
        this.desc = desc;
        this.op = op;
        switch (getOp()) {
            case 1:
                this.id = "CL";
                break;
            case 2:
                this.id = "RL";
                break;
            case 3:
                this.id = "CEL";
                break;
            case 4:
                this.id = "CR";
                break;
            case 5:
                this.id = "REL";
                break;
            case 6:
                this.id = "EM";
                break;
            case 7:
                this.id = "CLN";
                break;
            case 8:
                this.id = "CERR";
                break;
        }
        this.id += Main.tAux++;
    }

    public String getId() {
        return id;
    }

    public String getRuta1() {
        return ruta1;
    }

    public String getRuta2() {
        return ruta2;
    }

    public int getOp() {
        return op;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static void ingresarTarea(Tarea t) {
        DefaultTableModel m = (DefaultTableModel) tblTareas.getModel();
        m.addRow(new Object[]{t.getId(), t.getDesc()});
        Main.tareas.add(t);
        Main.lblTareas.setText(Main.btnFormatear.getText().equals("Formatear") ? "Tareas que se estan realizando: " + Main.tareas.size() : "Tasks that are being carried out: " + Main.tareas.size());
    }

    public static void quitarTarea(String id) {         
        DefaultTableModel m = (DefaultTableModel) tblTareas.getModel();
        for (int i = 0; i < Main.tareas.size(); i++) {
            if (Main.tareas.get(i).getId().equals(id)) {
                Main.tareas.remove(i);
                break;
            }
        } 
        Main.lblTareas.setText(Main.btnFormatear.getText().equals("Formatear") ? "Tareas que se estan realizando: " + Main.tareas.size() : "Tasks that are being carried out: " + Main.tareas.size());
               for (int i = 0; i < Main.tblTareas.getRowCount(); i++) {
            if (Main.tblTareas.getValueAt(i, 0).equals(id)) {
                tblTareas.setRowSelectionInterval(i, i);
                m.removeRow(tblTareas.getSelectedRow());
                break;
            }
        }
    }

}
