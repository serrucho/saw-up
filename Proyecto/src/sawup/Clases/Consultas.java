package sawup.Clases;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Consultas {

    final static String BORRAR_COPIA = "DELETE FROM backup WHERE nombreArchivo=? AND FK_usuario=?";
    final static String CAMBIO_CLAVE = "UPDATE usuario SET password = sha2(?, 512) WHERE email = ?";
    final static String CAMBIO_NOMBRE_APELLIDOS = "UPDATE usuario SET nombre = ?, apellidos = ? WHERE email = ?";
    final static String COMPROBAR_VERSION = "SELECT version.version, version.nombre, version.archivo FROM version ORDER BY version.releaseDate DESC LIMIT 1";
    final static String CONTEO_COPIA = "SELECT COUNT(*) FROM backup WHERE FK_usuario=?";
    final static String CONTEO_USUARIO = "SELECT COUNT(*) FROM usuario WHERE usuario.email=?";
    final static String INSERTAR_COPIA = "INSERT INTO backup(FK_usuario, nombreArchivo, archivo, fechaCopia, horaCopia, hash) VALUES (?, ?, ?,  CURDATE(),?, ?)";
    final static String INSERTAR_PROBLEMA = "INSERT INTO problema (codigo, informacion, fecha, hora, FK_usuario) VALUES (?, ?, CURDATE(), ?, ?)";
    final static String REGISTRO_INICIO = "INSERT INTO inicio (FK_usuario, dia, hora, IPpublica, nombreEquipo, osVersion, cuentaLocal) VALUES (?, CURDATE(), ?, ?, ?, ?, ?)";
    final static String RESTAURAR_COPIA = "SELECT backup.nombreArchivo, backup.archivo FROM backup WHERE backup.nombreArchivo=? AND backup.FK_usuario=?";
    final static String SELECCIONAR_F_ACTU = "SELECT version.archivo FROM version WHERE version.nombre=?";
    final static String SELECCIONAR_USUARIO = "SELECT P_id, email, nombre, apellidos, password FROM usuario WHERE email LIKE ?";
    final static String SELECTOR_COPIAS = "SELECT * FROM backup WHERE FK_usuario=?";

    public static String getSELECCIONAR_USUARIO() {
        return SELECCIONAR_USUARIO;
    }

    public static String getREGISTRO_INICIO() {
        return REGISTRO_INICIO;
    }

    public static String getCAMBIO_CLAVE() {
        return CAMBIO_CLAVE;
    }

    public static String getINSERTAR_COPIA() {
        return INSERTAR_COPIA;
    }

    public static String getCAMBIO_NOMBRE_APELLIDOS() {
        return CAMBIO_NOMBRE_APELLIDOS;
    }

    public static String getCONTEO_COPIA() {
        return CONTEO_COPIA;
    }

    public static String getSELECTOR_COPIAS() {
        return SELECTOR_COPIAS;
    }

    public static String getCOMPROBAR_VERSION() {
        return COMPROBAR_VERSION;
    }

    public static String getBORRAR_COPIA() {
        return BORRAR_COPIA;
    }

    public static String getCONTEO_USUARIO() {
        return CONTEO_USUARIO;
    }

    public static String getINSERTAR_PROBLEMA() {
        return INSERTAR_PROBLEMA;
    }

    public static String getSELECCIONAR_F_ACTU() {
        return SELECCIONAR_F_ACTU;
    }

    public static String getRESTAURAR_COPIA() {
        return RESTAURAR_COPIA;
    }

}
