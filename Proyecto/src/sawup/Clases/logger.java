package sawup.Clases;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.*;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class logger {

    Date fecha = new Date();
    Logger log;

    public logger(String workspace) throws IOException {
        log = Logger.getLogger(logger.class);
        SimpleDateFormat formato = new SimpleDateFormat("dd.MM.yyyy");
        String fechaAc = formato.format(fecha);
        PatternLayout defaultLayout = new PatternLayout("%p: %d{HH:mm:ss} –> %m%n");
        RollingFileAppender rollingFileAppender = new RollingFileAppender();
        rollingFileAppender.setFile(workspace + "/log_" + fechaAc + ".log", true, false, 0);
        rollingFileAppender.setLayout(defaultLayout);
        log.removeAllAppenders();
        log.addAppender(rollingFileAppender);
        log.setAdditivity(false);
    }

    public Logger getLog() {
        return log;
    }

    public void setLog(Logger log) {
        this.log = log;
    }
}
