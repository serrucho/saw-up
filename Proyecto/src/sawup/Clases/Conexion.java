package sawup.Clases;

import java.io.IOException;
import java.sql.*;
import sawup.Metodos.Metodos;

/**
 *
 * @author Francisco Sierra Luciarte
 */
public class Conexion {

    String BD = "sawup";
    String LOGIN = "sawup";
    String PASS = "admin";
    public Connection conexion;

    public Conexion() {
    }

    public Connection conexionMySQL() {
        conexion = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection("jdbc:mysql://" + Paths.getIP() + ":3306/" + BD, Metodos.getAjuste(Paths.getCONP(), "mysql"), Metodos.getAjuste(Paths.getCONP(), "mysql2"));
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {

        } catch (IOException ex) {

        }
        return conexion;
    }
}
