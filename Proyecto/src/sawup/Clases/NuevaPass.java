package sawup.Clases;

import sawup.Metodos.Metodos;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class NuevaPass {

    public static String NUMEROS = "0123456789";
    public static String MAYUSCULAS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static String MINUSCULAS = "abcdefghijklmnopqrstuvwxyz";
    public static String ESPECIALES = "!?";

    public static String getPinNumber() {
        return getPassword(NUMEROS, 4);
    }

    public static String getPassword() {
        return getPassword(8);
    }

    public static String getPassword(int length) {
        return getPassword(NUMEROS + MAYUSCULAS + MINUSCULAS, length);
    }

    public static String getPassword(String key, int length) {
        String pswd = "";
        while (Metodos.passwordTest(pswd)==false) {
            for (int i = 0; i < length; i++) {
                pswd += (key.charAt((int) (Math.random() * key.length())));
            }
        }
        return pswd;
    }

}
