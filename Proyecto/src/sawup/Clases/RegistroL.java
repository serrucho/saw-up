package sawup.Clases;

import java.io.Serializable;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class RegistroL implements Serializable {

    int id;
    String nombreCopia;
    String contraseña;
    String[] rutasFicheros;
    String rutaDestino;
    String informacion;
    String fecha;
    String hora;
    String hash;
    String prop;
    String password;

    public RegistroL(int id, String nombreCopia, String[] rutasFicheros, String rutaDestino, String informacion, String fecha, String hora, String hash, String contraseña, String prop, String password) {
        this.id = id;
        this.nombreCopia = nombreCopia;
        this.rutasFicheros = rutasFicheros;
        this.rutaDestino = rutaDestino;
        this.informacion = informacion;
        this.fecha = fecha;
        this.hora = hora;
        this.hash = hash;
        this.contraseña = contraseña;
        this.prop = prop;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getNombreCopia() {
        return nombreCopia;
    }

    public String getContraseña() {
        return contraseña;
    }

    public String[] getRutasFicheros() {
        return rutasFicheros;
    }

    public String getRutaDestino() {
        return rutaDestino;
    }

    public String getInformacion() {
        return informacion;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getHash() {
        return hash;
    }

    public String getProp() {
        return prop;
    }

    public void setRutaDestino(String rutaDestino) {
        this.rutaDestino = rutaDestino;
    }

    public String getPassword() {
        return password;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

}
