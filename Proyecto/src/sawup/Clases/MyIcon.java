package sawup.Clases;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class MyIcon implements Icon {

    String ruta;

    public MyIcon(String ruta) {
        this.ruta = ruta;
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        Image image = new ImageIcon(getClass().getResource(getRuta())).getImage();
        g.drawImage(image, x, y, c);
    }

    @Override
    public int getIconWidth() {
        return 75;
    }

    @Override
    public int getIconHeight() {
        return 75;
    }

    public String getRuta() {
        return ruta;
    }

}
