package format;

import com.placeholder.PlaceHolder;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class Text {

    static final String fontPrincipal = "Microsoft JhengHei";
    static final int tamañoPricipal = 18;

    static final String mainFolder = "./files";
    static final String configFolder = mainFolder + "/config";
    static final String languageFolder = mainFolder + "/language";
    static final String startFile = configFolder + "/config.properties";
    static final String spanishFile = languageFolder + "/spanish.properties";
    static final String englishFile = languageFolder + "/english.properties";


    public static void text(PlaceHolder holder) {
        Properties p = new Properties();
        p = comprobarLenguaje(p);
        Formatear.lblTipoFormat.setText(p.getProperty("lblTipoFormat"));
        Formatear.btnFormateoRapido.setText(p.getProperty("btnFormateoRapido"));
        Formatear.btnFormateoCompleto.setText(p.getProperty("btnFormateoCompleto"));
        Formatear.lblTipoParticion.setText(p.getProperty("lblTipoParticion"));
        Formatear.btnHacerFormateo.setText(p.getProperty("btnHacerFormateo"));
        holder = new PlaceHolder(Formatear.campoNombre, Color.gray, Color.black, p.getProperty("campoNombre"), false, getFontPrincipal(), getTamañoPricipal());
    }

    public static Properties comprobarLenguaje(Properties p) {
        try {
            p.load(new FileReader(getStartFile()));
            switch (p.getProperty("language")) {
                case "spanish":
                    p.load(new FileReader(getSpanishFile()));
                    break;
                case "english":
                    p.load(new FileReader(getEnglishFile()));
                    break;
                default:
                    p.load(new FileReader(getSpanishFile()));
                    break;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Text.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }

    public static String getFontPrincipal() {
        return fontPrincipal;
    }

    public static int getTamañoPricipal() {
        return tamañoPricipal;
    }

    public static String getMainFolder() {
        return mainFolder;
    }

    public static String getConfigFolder() {
        return configFolder;
    }

    public static String getLanguageFolder() {
        return languageFolder;
    }

    public static String getStartFile() {
        return startFile;
    }

    public static String getSpanishFile() {
        return spanishFile;
    }

    public static String getEnglishFile() {
        return englishFile;
    }


}
