package format;

import static format.Formatear.btnHacerFormateo;
import static format.Formatear.pb;
import java.awt.HeadlessException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author DAM 224 - Francisco sierra Luciarte
 */
public class T extends Thread {

    String drive;
    String speed;
    String type;
    String name;
    String cmd = "formatter.bat";

    public T(String drive, String speed, String type, String name) {
        this.drive = drive;
        this.speed = speed;
        this.type = type;
        this.name = name;
    }

    public String getDrive() {
        return drive;
    }

    public String getSpeed() {
        return speed;
    }

    public String getType() {
        return type;
    }

    public String getNameD() {
        return name;
    }

    public String getCmd() {
        return cmd;
    }

    @Override
    public void run() {
        try {
            Formatear.ocupado = true;
            btnHacerFormateo.setEnabled(false);
            FileWriter fw = new FileWriter(getCmd());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("Format /y" + " " + getDrive() + getSpeed() + " " + getType() + " " + "/v:" + getNameD());
            System.out.println("Format /y" + " " + getDrive() + getSpeed() + " " + getType() + " " + "/v:" + getNameD());
            bw.close();
            pb.setIndeterminate(true);
            pb.setStringPainted(true);
            pb.setString(btnHacerFormateo.getText().equals("Formatear") ? "Formateando..." : "Formatting...");
            Process p = null;
            try {
                p = Runtime.getRuntime().exec(getCmd());
                p.waitFor();
            } catch (InterruptedException ex) {
                System.out.println(ex);
            } catch (Error e) {
                System.out.println(e);
            }
            new File(getCmd()).delete();
            pb.setIndeterminate(false);
            btnHacerFormateo.setEnabled(true);
            btnHacerFormateo.setText(btnHacerFormateo.getText().equals("Formatear") ? "Salir" : "Exit");
            if (p.exitValue() != 0) {
                JOptionPane.showMessageDialog(null, btnHacerFormateo.getText().equals("Salir") ? "Ha ocurrido un error mientras se formateaba su dispositivo, estos errores pueden ser que el dispositivo este siendo utilizado por otro programa o que se haya desconectado de forma accidental, reviselo y formatealo de nuevo." : "An error has occurred while formatting your device, these errors could be that the device is being used by another program or that it has been accidentally disconnected, check it and format it again.", "Saw Up", JOptionPane.ERROR_MESSAGE, Formatear.icono);
                pb.setString("Error");
            } else {
                pb.setString(btnHacerFormateo.getText().equals("Salir") ? "¡Formateado!" : "¡Formatted!");
            }
        } catch (IOException e) {
            System.out.println(e);
        } catch (HeadlessException ex) {
            Logger.getLogger(T.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Formatear.ocupado = false;
        }
    }
}
